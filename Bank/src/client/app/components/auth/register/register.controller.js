angular.module('app.register')
    .controller('RegisterController', ['$scope', '$state', 'authService', RegisterController]);

function RegisterController($scope, $state, authService) {
    $scope.$parent.vm.a = 2;
    var vm = this;
    vm.gender = 'Male';
    vm.register = function() {
        if (vm.password !== vm.password_confirm) {
            toastr.error("Password confirm not match");
        } else {
            var newUser = {
                email: vm.email,
                password: vm.password,
                username: vm.username,
                typeCard: vm.typeCard,
                gender: vm.gender,
                phone: vm.phone,
                // reCaptcha: grecaptcha.getResponse()
            };
            return authService.register(newUser).then(
                function(res) {
                    toastr.success(res);
                    $state.go('auth.login');
                },
                function(err) {
                    toastr.error(err);
                }
            );
        }

    };
}