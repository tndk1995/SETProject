angular.module('app.activateAccount')
    .config(activateAccountConfig);

function activateAccountConfig($stateProvider) {
    $stateProvider
        .state('auth.activateAccount', {
            url: '/activateAccount/:token',
            resolve: {
                active: function(authService, $stateParams, $state) {
                    return authService.activateAccount({ token: $stateParams.token })
                        .then(res => {
                            toastr.success(res);
                            $state.go('auth.login');
                        }, err => {
                            toastr.error(err);
                        });
                }
            }
        });
}