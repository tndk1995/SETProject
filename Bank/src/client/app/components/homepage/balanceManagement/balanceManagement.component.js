angular.module('app.homepage')
    .component('balanceManagement', {
        bindToController: true,
        templateUrl: 'app/components/homepage/balanceManagement/balanceManagement.html',
        controller: balanceManagementController,
        controllerAs: 'vm',
        bindings: {
            user: '=',
            namePage: '=',
            role: '='
        }
    });

balanceManagementController.$inject = ['homeService', '$state'];

function balanceManagementController(homeService, $state) {
    var vm = this;
    vm.changePIN = false;
    vm.edit = edit;
    vm.cancel = cancel;
    vm.getCert = getCert;

    function edit() {
        var update = {
            _id: vm.user._id,
            balance: vm.balance
        };
        if (vm.changePIN) {
            update.oldPIN = vm.oldPIN;
            update.newPIN = vm.pin;
        }
        return homeService.updateBalance(update).then(
            function(res) {
                toastr.success(res);
                $state.reload();
            },
            function(err) {
                toastr.error(err);
            }
        );
    }

    function cancel() {
        $state.reload();
    }

    function getCert() {
        homeService.getCert().then(
            function(res) {
                toastr.success(res);
            },
            function(err) {
                toastr.error(err);
            }
        );
    }

}