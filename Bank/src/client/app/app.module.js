(function() {
    'use strict';

    angular.module('app', [
        'app.auth',
        'app.homepage',

        'tokenInterceptor.service',
        'services.auth',
        'ui.router',
        'angular-jwt',
        'ngStorage',
        'ngAnimate',
        'ngSanitize',
        'ngplus',
        'app.auth',
        'blocks.exception',
        'blocks.logger',
        'blocks.router'
    ]);

})();