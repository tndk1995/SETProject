(function() {

    angular.module('services.auth', ['ngStorage', 'services.errorTranslator'])
        .factory('authService', ['$q', '$http', '$localStorage', '$sessionStorage', 'jwtHelper', 'errTransService', authService]);

    function authService($q, $http, $localStorage, $sessionStorage, jwtHelper, errTransService) {

        var storage;

        function login(request, state) {
            var deferred = $q.defer();

            if (state === 2) {
                return $localStorage.bankToken ? true : false;
            }

            $http.post('api/auth/signin/' + state, request)
                .then(function(res) {
                    $localStorage.bankToken = res.data.token;
                    deferred.resolve('Login successful');
                }, function(err) {
                    deferred.reject(errTransService[err.data.message]);
                });

            return deferred.promise;
        }

        function register(newUser) {
            var deferred = $q.defer();
            if (newUser) {
                $http.post('api/auth/signup', newUser)
                    .then(function(res) {
                        deferred.resolve('Register successful! Check your email to activate your account');
                    }, function(err) {
                        deferred.reject(errTransService[err.data.message]);
                    });
            }
            return deferred.promise;
        }

        function logout() {
            delete $localStorage.bankToken;
            return 'Logout successful';
        }

        function sendMail(email) {
            var deferred = $q.defer();
            if (email) {
                $http.post('api/auth/sendEmail', email)
                    .then(function(res) {
                        deferred.resolve('Sent');
                    }, function(err) {
                        deferred.reject(errTransService[err.data.message]);
                    });
            }
            return deferred.promise;
        }

        function resetPassword(request) {
            var deferred = $q.defer();
            if (request) {
                $http.post('api/auth/resetPassword', request)
                    .then(function(res) {
                        deferred.resolve('Changed successfully');
                    }, function(err) {
                        deferred.reject(errTransService[err.data.message]);
                    });
            }
            return deferred.promise;
        }

        function getDecodedToken() {
            return jwtHelper.decodeToken($localStorage.bankToken);
        }

        function getToken() {
            return $localStorage.bankToken;

        }

        function activateAccount(token) {
            var deferred = $q.defer();
            if (token) {
                $http.post('api/auth/activateAccount', token)
                    .then(function(res) {
                        deferred.resolve('Activated successfully');
                    }, function(err) {
                        deferred.reject(errTransService[err.data.message]);
                    });
            }
            return deferred.promise;
        }

        return {
            login: login,
            logout: logout,
            register: register,
            storage: storage,
            sendMail: sendMail,
            resetPassword: resetPassword,
            getToken: getToken,
            getDecodedToken: getDecodedToken,
            activateAccount: activateAccount
        };

    }
})();