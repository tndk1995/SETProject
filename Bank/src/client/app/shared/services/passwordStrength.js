angular.module('services.auth')
    .directive('passwordStrength', passwordStrength);

function passwordStrength() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            var weak = new RegExp("^((?=.*[a-z])|(?=.*[0-9]))(?=.{6,})");
            var medium = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");
            var good = new RegExp("^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]))(?=.{6,})");
            var strong = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");

            function checkPassword(pass) {
                if (strong.test(pass)) {
                    return 4;
                } else if (good.test(pass)) {
                    return 3;
                } else if (medium.test(pass)) {
                    return 2;
                } else if (weak.test(pass)) {
                    return 1;
                } else
                    return 0;
            }

            function passwordStrength(value) {
                var valid = value ? value.length >= 6 : false;
                scope.vm.pStrength = value ? checkPassword(value) : 0;
                ngModelCtrl.$setValidity('passwordStrength', valid);
                return value;
            }
            ngModelCtrl.$parsers.push(passwordStrength);
            ngModelCtrl.$formatters.push(passwordStrength);

            scope.$watch(attrs.passwordStrength, function() {
                ngModelCtrl.$setViewValue(ngModelCtrl.$viewValue);
            });
        }
    };
}