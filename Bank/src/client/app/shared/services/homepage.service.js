 angular.module('services.hompage', ['services.errorTranslator', 'services.auth'])
     .factory('homeService', ['$q', '$http', 'errTransService', 'authService', homeService]);

 function homeService($q, $http, errTransService, authService) {
     return {
         getUser: getUser,
         updateUser: updateUser,
         getAllUser: getAllUser,
         deleteUser: deleteUser,
         updateBalance: updateBalance,
         getCert: getCert
     }

     function getUser() {
         var deferred = $q.defer();
         $http.get('api/users')
             .then(function(res) {
                 deferred.resolve(res);
             }, function(err) {
                 deferred.reject(errTransService[err.data.message]);
             });

         return deferred.promise;
     }

     function getAllUser(pageIndex) {
         var deferred = $q.defer();

         $http.get('api/users/' + pageIndex)
             .then(function(res) {
                 deferred.resolve(res);
             }, function(err) {
                 deferred.reject(errTransService[err.data.message]);
             });

         return deferred.promise;
     }

     function updateUser(newUser) {
         var deferred = $q.defer();
         if (newUser) {
             $http.put('api/users', newUser)
                 .then(function(res) {
                     deferred.resolve('Update successfully');
                 }, function(err) {
                     deferred.reject(errTransService[err.data.message]);
                 });
         }
         return deferred.promise;
     }

     function updateBalance(newBalance) {
         var deferred = $q.defer();
         if (newBalance) {
             $http.put('api/users/balance', newBalance)
                 .then(function(res) {
                     deferred.resolve('Update successfully');
                 }, function(err) {
                     deferred.reject(errTransService[err.data.message]);
                 });
         }
         return deferred.promise;
     }

     function deleteUser(id) {
         var deferred = $q.defer();

         $http.delete('api/users/' + id)
             .then(function(res) {
                 deferred.resolve('Delete successfully');
             }, function(err) {
                 deferred.reject(errTransService[err.data.message]);
             });

         return deferred.promise;
     }

     function getCert() {
         var deferred = $q.defer();
         $http.post('api/users/certificate', {})
             .then(function(res) {
                 deferred.resolve('Sent successfully');
             }, function(err) {
                 deferred.reject(err.data.message);
             });

         return deferred.promise;
     }
 }