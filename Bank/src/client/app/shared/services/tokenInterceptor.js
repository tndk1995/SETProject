angular.module('tokenInterceptor.service', []).factory('tokenInterceptor', tokenInterceptor)

tokenInterceptor.$inject = ['$localStorage', '$sessionStorage'];

function tokenInterceptor($localStorage, $sessionStorage) {
    return {
        request: function(config) {
            var token;
            if ($localStorage.bankToken) {
                token = $localStorage.bankToken;

            }
            if (token) {
                config.headers['x-access-token'] = token;
            }
            return config;
        }

    }
};