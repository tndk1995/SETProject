var jwt = require('jsonwebtoken');
var config = require('../config/config');
var userDao = require('../dao/user.dao');
var failMessage = require('./../services/failMessage');
module.exports = {
    parser: parser
};

function parser() {
    var role = Array.prototype.slice.call(arguments);
    return function(req, res, next) {
        var token = req.headers['x-access-token'] || req.body.token;
        // decode token
        if (token) {
            // verifies secret and checks exp
            return new Promise(function(resolve, reject) {
                jwt.verify(token, config.pubKey, { algorithms: ['RS256'] }, function(err, decoded) {
                    if (err) {
                        console.log(err);
                        if (err.name == 'TokenExpiredError') {
                            return reject(res.status(500).send({ message: failMessage.user.jwt.expired }).end());
                        }
                        return reject(res.status(500).send({ message: failMessage.user.jwt.unauthorized }).end());
                    } else {
                        var email = decoded.email;
                        //If is User, get from database
                        if (decoded.role === 'user' && role.indexOf('user') > -1) {
                            userDao.getUserByEmail(email)
                                .then((user) => {
                                    req.decoded = decoded;
                                    next();
                                }).catch((err) => {
                                    return reject(err);
                                });

                        } else if (decoded.role === 'admin' && role.indexOf('admin') > -1) {
                            req.decoded = decoded;
                            next();
                        } else {
                            return reject(res.status(500).send({ message: failMessage.user.jwt.unauthorized }).end());

                        }
                    }
                });
            });
        } else {
            // if there is no token
            return res.status(403).send({
                message: failMessage.user.jwt.noToken
            });
        }
    }
}