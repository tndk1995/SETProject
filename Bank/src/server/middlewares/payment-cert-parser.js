var jwt = require('jsonwebtoken');
var config = require('../config/config');
var userDao = require('../dao/user.dao');
var failMessage = require('./../services/failMessage');
var Payment = require("../model/payment.model");

module.exports = {
    requestPaymentParser: requestPaymentParser,
    paymentParser: paymentParser
}

function requestPaymentParser() {
    return function(req, res, next) {
        var cardHolderToken = req.body.cardHolderCert;
        var merchantToken = req.body.merchantCert;
        // decode token
        if (cardHolderToken && merchantToken) {
            // verifies secret and checks exp
            return new Promise(function(resolve, reject) {
                jwt.verify(cardHolderToken, config.CApubKey, { algorithms: ['RS256'] }, function(err, decoded) {
                    if (err) {
                        console.log("cardHolderTokenErr");
                        if (err.name == 'TokenExpiredError') {
                            return reject(res.status(500).send({ message: failMessage.user.jwt.expired }).end());
                        }
                        return reject(res.status(500).send({ message: failMessage.user.jwt.unauthorized }).end());
                    } else {
                        req.body.cardHolderCert = decoded;
                    }
                });
                jwt.verify(merchantToken, config.CApubKey, { algorithms: ['RS256'] }, function(err, decoded) {
                    if (err) {
                        console.log("merchantTokenErr");
                        if (err.name == 'TokenExpiredError') {
                            return reject(res.status(500).send({ message: failMessage.user.jwt.expired }).end());
                        }
                        return reject(res.status(500).send({ message: failMessage.user.jwt.unauthorized }).end());
                    } else {
                        req.body.merchantCert = decoded;
                    }
                });
                next();
            });
        } else {
            // if there is no token
            return res.status(403).send({
                message: failMessage.user.jwt.noToken
            });
        }
    }
}

function paymentParser() {
    return function(req, res, next) {
        var token = req.body.paymentToken;
        // decode token
        if (token) {
            // verifies secret and checks exp
            return new Promise(function(resolve, reject) {
                jwt.verify(token, config.pubKey, { algorithms: ['RS256'] }, function(err, decoded) {
                    if (err) {
                        console.log("paymentTokenErr");
                        if (err.name == 'TokenExpiredError') {
                            return reject(res.status(500).send({ message: failMessage.user.jwt.expired }).end());
                        }
                        return reject(res.status(500).send({ message: failMessage.user.jwt.unauthorized }).end());
                    } else {

                        Payment.findById(decoded._id).then((payment) => {
                            if (!payment) {
                                return reject(res.status(500).send({ message: failMessage.user.payment.notFound }).end());
                            } else if (!payment.status) {
                                return reject(res.status(500).send({ message: failMessage.user.payment.inProgress }).end());
                            }
                            payment.status = false;
                            payment.save()
                                .then((res) => {
                                    req.body.payment = res;
                                    next()
                                })
                                .catch(err => {
                                    return reject(err);
                                })

                        })
                    }
                });
            });
        } else {
            // if there is no token
            return res.status(403).send({
                message: failMessage.user.jwt.noToken
            });
        }
    }
}