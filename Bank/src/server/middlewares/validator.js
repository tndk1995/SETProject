var validateRegister = require("./../model/validateRegister.model");
var validateLogin = require("./../model/validateLogin.model");

module.exports = {
    validator: validator
};

function validator() {
    return function(req, res, next) {
        if (req.url == '/signup') {
            req.checkBody(validateRegister);
        } else if (req.url.slice(0, 7) == '/signin') {
            req.checkBody(validateLogin);

        }
        req.getValidationResult().then(function(result) {
            if (!result.isEmpty()) {
                return res.status(500).send({ message: result.array()[0].msg }).end(); //result.mapped()
            } else {
                next();
            }
        });

    }
}