/*jshint esversion: 6*/
var User = require('./../model/user.model');
var cryptoPasswordUtil = require('./../utils/cryptoPassword');
var successMessage = require('../services/successMessage');
var failMessage = require('../services/failMessage');
var jwtDecode = require('jwt-decode');
var pagination = require('../services/pagination');
var generateKey = require('../utils/generateKey');
var email = require('../utils/email');
var config = require('../config/config');
var forge = require('node-forge');
var CryptoJS = require("crypto-js");

module.exports = {
    createUser: createUser,
    changePasswordUser: changePasswordUser,
    updateUser: updateUser,
    convertUserModelToUserResponse: convertUserModelToUserResponse,
    getUserByEmail: getUserByEmail,
    getAllUser: getAllUser,
    deleteUser: deleteUser,
    updateBalance: updateBalance,
    getCertificate: getCertificate,
};

function convertUserModelToUserResponse(userModel) {
    var userObj = userModel.toObject();
    delete userObj.password;
    delete userObj.salt;
    delete userObj.PIN;
    return userObj;
}

function createUser(request) {
    var password = cryptoPasswordUtil.cryptoPassword(request.password);
    return User.findOne({ email: request.email }).then((user) => {
            if (user) {
                return Promise.reject({
                    statusCode: 400,
                    message: failMessage.user.signup.duplicateUser
                });
            }
            var keys = generateKey();
            var PIN = Math.floor((Math.random() * 10000) + 1);
            var newUser = new User({
                email: request.email,
                password: password.hash,
                salt: password.salt,
                username: request.username,
                typeCard: request.typeCard,
                gender: request.gender,
                phone: request.phone,
                role: request.role,
                balance: request.balance,
                PIN: cryptoPasswordUtil.cryptoWithSalt(PIN, password.salt),
                publicKey: keys.publicKey,
                activate: false
            });
            return newUser.save()
                .then((response) => {
                    return generateBankId(response, keys.privateKey, PIN);
                }).catch(() => {
                    return Promise.reject({
                        message: failMessage.user.signup.systemErr
                    });
                });
        })
        .catch((err) => {
            return Promise.reject({
                statusCode: 400,
                message: failMessage.user.register.systemErr
            });
        });
}

function generateBankId(user, privateKey, PIN) {
    return User.findById({
            _id: user._id
        })
        .then((user) => {
            if (user) {
                user.bankId = user._id.toString().substring(0, 12);
                return user.save()
                    .then((res) => {
                        email.sendMailVerify(res.email, { bankId: res.bankId, typeCard: res.typeCard, PIN: PIN, privateKey: privateKey });
                        return Promise.resolve({
                            message: successMessage.user.signup,
                            // user: convertUserModelToUserResponse(res)
                        });
                    });
            }
        })
}

function changePasswordUser(request) {

    var token = request.token;
    var decoded = jwtDecode(token);
    var _id = decoded._id;
    //console.log(decoded);
    return User.findById({
            _id: _id
        })
        .then((user) => {
            if (user) {
                //console.log("user")
                if (cryptoPasswordUtil.verifyPassword(user.password, user.salt, request.passwordOld)) {
                    var passwordNew = cryptoPasswordUtil.cryptoPassword(request.passwordNew);
                    user.password = passwordNew.hash;
                    user.salt = passwordNew.salt;
                    //console.log(passwordNew);
                    return user.save()
                        .then((res) => {
                            return Promise.resolve({
                                message: successMessage.user.changePassword
                            });
                        })
                        .catch((err) => {
                            return Promise.reject({
                                message: failMessage.user.changePassword.systemErr
                            });
                        });
                } else {
                    return Promise.reject({
                        message: failMessage.user.changePassword.passwordOldNotCorrect
                    });
                }
            }
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}

function updateUser(request) {
    var decoded = jwtDecode(request.token);
    var role = decoded.role;
    return User.findById({
            _id: request._id
        })
        .then((user) => {
            // console.log(user);
            if (!user) {
                return Promise.reject({
                    statusCode: 404,
                    message: failMessage.user.login.notFound
                });
            }
            if (request.oldPass || role == "admin") {
                if (role == "admin" || cryptoPasswordUtil.verifyPassword(user.password, user.salt, request.oldPass)) {
                    var newPass = cryptoPasswordUtil.cryptoPassword(request.newPass);
                    user.password = newPass.hash;
                    user.salt = newPass.salt;
                } else {
                    return Promise.reject({
                        statusCode: 403,
                        message: failMessage.user.changePassword.passwordOldNotCorrect
                    });
                }
            }
            user.username = request.username || user.username;
            user.birthday = request.birthday || user.birthday;
            user.gender = request.gender || user.gender;
            user.phone = request.phone || user.phone;
            return user.save()
                .then((res) => {
                    return Promise.resolve({
                        message: successMessage.user.update
                    });
                })
                .catch((err) => {
                    console.log(err);
                    return Promise.reject({
                        message: failMessage.user.changePassword.systemErr
                    });
                });

        })
        .catch((err) => {
            return Promise.reject(err);
        });
}

function updateBalance(request) {
    return User.findById({
            _id: request._id
        })
        .then((user) => {
            // console.log(user);
            if (!user) {
                return Promise.reject({
                    statusCode: 404,
                    message: failMessage.user.login.notFound
                });
            }
            if (request.oldPIN) {
                if (cryptoPasswordUtil.verifyPIN(user.PIN, user.salt, request.oldPIN)) {
                    user.PIN = cryptoPasswordUtil.cryptoWithSalt(request.newPIN, user.salt);
                } else {
                    return Promise.reject({
                        statusCode: 403,
                        message: failMessage.user.changePassword.PINOldNotCorrect
                    });
                }
            }
            user.balance = request.balance || user.balance;
            return user.save()
                .then((res) => {
                    return Promise.resolve({
                        message: successMessage.user.update
                    });
                })
                .catch((err) => {
                    return Promise.reject({
                        message: failMessage.user.changePassword.systemErr
                    });
                });

        })
        .catch((err) => {
            return Promise.reject(err);
        });
}

function getAllUser(request) {
    var query = { role: 'user' };
    return User.count(query).then(function(count) {
        return User.find(query)
            .skip((request.pageIndex > 0) ? (request.pageIndex - 1) * request.pageSize : 0)
            .limit(request.pageSize)
            .exec().then(function(users) {
                for (var user in users) {
                    users[user].password = undefined;
                    users[user].salt = undefined;
                }
                var res = pagination.pagination(users, count, request.pageIndex, request.pageSize);
                return Promise.resolve(res);

            })
            .catch((err) => {
                return Promise.reject(err);
            });
    });
}

function getUserByEmail(email) {
    return User.findOne({ email: email })
        .then((user) => {
            if (!user) {
                return Promise.reject({
                    statusCode: 404,
                    message: failMessage.user.login.notFound
                });
            }
            return Promise.resolve({
                statusCode: 200,
                user: convertUserModelToUserResponse(user)
            });
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}

function getCertificate(requestEmail) {
    return User.findOne({ email: requestEmail })
        .then((user) => {
            if (!user) {
                return Promise.reject({
                    statusCode: 404,
                    message: failMessage.user.login.notFound
                });
            }
            email.sendMailBankPubkey(user.email, config.pubKey.toString());
            return Promise.resolve({
                message: successMessage.user.sendEmail
            });
        })
        .catch((err) => {
            console.log(err)
            return Promise.reject(err);
        });
}

function deleteUser(id) {
    return User.remove({ _id: id })
        .then((user) => {
            return Promise.resolve({
                message: successMessage.user.remove
            });
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}