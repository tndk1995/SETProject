/*jshint esversion: 6*/
var User = require('./../model/user.model');
var Payment = require("./../model/payment.model");
var cryptoPasswordUtil = require('./../utils/cryptoPassword');
var successMessage = require('../services/successMessage');
var failMessage = require('../services/failMessage');
var email = require('../utils/email');
var config = require('../config/config');
var forge = require('node-forge');
var CryptoJS = require("crypto-js");
var jwt = require('./../utils/jwt.util');

module.exports = {
    requestPayment: requestPayment,
    payment: payment
};

function requestPayment(request) {
    // request.cardHolderCert = jwtDecode(request.cardHolderCert);
    // request.merchantCert = jwtDecode(request.merchantCert);
    return User.find({ email: { $in: [request.cardHolderCert.email, request.merchantCert.email] } })
        .then((users) => {

            if (users.length != 2) {
                return Promise.reject({
                    statusCode: 404,
                    message: failMessage.user.login.notFound
                });
            } else {
                var privateKey = forge.pki.privateKeyFromPem(config.certKey);
                var tempKey = privateKey.decrypt(request.DigitalEnvelope);
                var PIPacket = JSON.parse(CryptoJS.AES.decrypt(request.PIPacketCipher, tempKey).toString(CryptoJS.enc.Utf8));
                var cardHolder = users.filter((user) => user.email == request.cardHolderCert.email)[0];
                var merchant = users.filter((user) => user.email == request.merchantCert.email)[0];

                var PIMD = CryptoJS.SHA1(JSON.stringify(PIPacket.paymentInformation)).toString();
                var POMD = CryptoJS.SHA1(PIPacket.OIMD + PIMD);
                var cardHolderPubKey = forge.pki.publicKeyFromPem(cardHolder.publicKey);
                var md = forge.md.sha1.create();
                md.update(POMD);
                var resultCompare = cardHolderPubKey.verify(md.digest().bytes(), forge.util.decode64(PIPacket.DualSignature));
                if (resultCompare &&
                    cardHolder.bankId == PIPacket.paymentInformation.bankID &&
                    cryptoPasswordUtil.verifyPIN(cardHolder.PIN, cardHolder.salt, PIPacket.paymentInformation.PIN) &&
                    cardHolder.typeCard == PIPacket.paymentInformation.typeOfPayment
                ) {
                    var newPayment = new Payment({
                        cardHolderId: cardHolder._id,
                        merchantId: merchant._id,
                        status: true,
                        createAt: new Date(),
                        paymentId: PIPacket.paymentInformation.paymentId
                    });

                    return newPayment.save()
                        .then((payment) => {
                            var paymentToken = jwt.signTokenWithExpire(convertPaymentModelToObject(payment), 60 * 5);
                            return Promise.resolve({
                                message: paymentToken
                            });
                        });
                } else {
                    return Promise.reject({
                        statusCode: 400,
                        message: failMessage.user.payment.wrongInfo
                    });
                }

            }
        }).catch((err) => {
            console.log(err)
            return Promise.reject(err);
        });
}

function payment(request) {
    return User.find({ _id: { $in: [request.payment.merchantId, request.payment.cardHolderId] } })
        .then((users) => {
            if (users.length != 2) {
                return Promise.reject({
                    statusCode: 404,
                    message: failMessage.user.login.notFound
                });
            }
            var cardHolder = users.filter((user) => user._id == request.payment.cardHolderId)[0];
            var merchant = users.filter((user) => user._id == request.payment.merchantId)[0];
            /**check OI */
            var merchantPublicKey = forge.pki.publicKeyFromPem(merchant.publicKey);
            var md = forge.md.sha1.create();
            md.update(request.orderInformation);
            var result = merchantPublicKey.verify(md.digest().bytes(), forge.util.decode64(request.encryptedOI));


            if (!result ||
                request.orderInformation.paymentId != request.payment.paymentId
            ) {
                return Promise.reject({
                    statusCode: 400,
                    message: failMessage.user.payment.wrongInfo
                });
            }
            var totalCostOI = parseInt(request.orderInformation.totalCost);
            if (cardHolder.balance < totalCostOI) {
                return Promise.reject({
                    statusCode: 400,
                    message: failMessage.user.payment.userBalance
                });
            }
            cardHolder.balance -= totalCostOI;
            merchant.balance += totalCostOI;
            return cardHolder.save()
                .then((cardHolder) => {
                    return merchant.save()
                        .then((merchant) => {
                            email.sendMailCompletePayment(merchant.email, cardHolder.email, {
                                totalCost: request.orderInformation.totalCost,
                                paymentId: request.orderInformation.paymentId,
                                merchantBalance: merchant.balance,
                                cardHolderBalance: cardHolder.balance
                            });
                            return Promise.resolve({
                                message: successMessage.user.payment
                            });
                        });

                }).catch((err) => {
                    return Promise.reject({
                        statusCode: 400,
                        message: failMessage.user.payment.systemErr
                    });
                })
        }).catch((err) => {
            console.log(err)

            return Promise.reject(err);
        });
}

function convertPaymentModelToObject(paymentModel) {
    var paymentObj = paymentModel.toObject();
    delete paymentObj.paymentId;
    delete paymentObj.merchantId;
    delete paymentObj.cardHolderId;
    return paymentObj;
}