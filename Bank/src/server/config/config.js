var fs = require('fs');
var path = require('path');
var rootPath = __dirname;
var variables = JSON.parse(fs.readFileSync(path.join(rootPath, 'config.json'), 'utf8'));
variables.options = {
    key: fs.readFileSync(path.join(rootPath, 'server.key')),
    cert: fs.readFileSync(path.join(rootPath, 'server.crt'))
};
variables.certKey = fs.readFileSync(path.join(rootPath, 'bankKey.pem'));
variables.pubKey = fs.readFileSync(path.join(rootPath, 'bankKey.pub'));

variables.CAcertKey = fs.readFileSync(path.join(rootPath, 'mykey.pem'));
variables.CApubKey = fs.readFileSync(path.join(rootPath, 'mykey.pub'));

module.exports = variables;