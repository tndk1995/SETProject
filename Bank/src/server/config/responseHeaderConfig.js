'use strict';
var helmet = require('helmet');

function responseHeaderConfig(app) {
    app.use(helmet.hsts({
        maxAge: 1000 * 60 * 60 * 24 * 365,
        includeSubdomains: true,
        preload: true
    }));
    app.use(helmet.contentSecurityPolicy({
        directives: {
            // defaultSrc: ["'none'"],
            scriptSrc: ["'self'", "https", "'unsafe-inline'", 'https://use.fontawesome.com', 'https://www.google.com', 'https://www.gstatic.com'],
            styleSrc: ["'self'", "https", "'unsafe-inline'", "https://fonts.googleapis.com", 'https://use.fontawesome.com'],
            imgSrc: ["'self'", "https", 'data:'],
            fontSrc: ["'self'", "https", "https://fonts.gstatic.com", 'https://use.fontawesome.com', 'data:'],
            connectSrc: ["'self'", "https"],
            reportUri: '/api/report/cspviolation'
        }
    }));
    app.use(helmet.xssFilter());
    app.use(helmet.frameguard({ action: 'deny' }));
    app.use(helmet.noSniff());
}
module.exports = responseHeaderConfig;