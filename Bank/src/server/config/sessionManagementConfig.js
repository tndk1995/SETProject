/*jshint esversion: 6*/
"use strict";
var db = require('../db/db.config');
var config = require('../config/config');
var session = require('express-session');
var mongoStoreFactory = require('connect-mongo');

function sessionManagementConfig(app) {
    var MongoStore = mongoStoreFactory(session);

    app.use(session({
        store: new MongoStore({
            mongooseConnection: db,
            ttl: (1 * 60)
        }),
        secret: config.secretPassword,
        saveUninitialized: true,
        resave: false,
        cookie: {
            path: "/",
            httpOnly: true,
            secure: true,
            maxAge: (1 * 60 * 1000)
        },
        name: "id"
    }));

    session.Session.prototype.login = function(res) {
        var req = this.req;
        return new Promise(function(resolve, reject) {
            req.session.regenerate(function(err) {
                if (err) {
                    return reject(err);
                }
                req.session.response = res;
                return resolve();
            })

        })

    };
}
module.exports = sessionManagementConfig;