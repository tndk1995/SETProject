var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        match: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i
    },
    password: {
        type: String,
        required: true,
        match: /(?=.*[a-zA-Z]+)(?=.*[0-9]+)(?=.*[!*&^%$#@()+]+).*/
    },
    salt: {
        type: String
    },
    username: {
        type: String
    },
    activate: {
        type: Boolean
    },
    gender: {
        type: String
    },
    phone: {
        type: String
    },
    role: {
        type: String
    },
    typeCard: {
        type: String
    },
    bankId: {
        type: String
    },
    PIN: {
        type: String
    },
    publicKey: {
        type: String
    },
    balance: {
        type: Number
    }
});
var user = mongoose.model('user', userSchema);

module.exports = user;