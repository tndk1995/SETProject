var registerSchema = {
    'email': {
        notEmpty: true,
        isEmail: {
            errorMessage: "INVALID_EMAIL"
        }
    },
    'password': {
        notEmpty: true,
        isLength: {
            options: [{ min: 6 }],
            errorMessage: "PASSWORD_LENGTH"
        },
        matches: {
            options: ["(?=.*[a-zA-Z])(?=.*[0-9]+).*", "g"],
            errorMessage: "PASSWORD_TYPE"
        }
    },
    'username': {
        notEmpty: true,
        isLength: {
            options: [{ max: 25 }],
            errorMessage: "USERNAME_LENGTH"
        },
        matches: {
            options: ["^[a-z0-9 ._-]+$", "i"],
            errorMessage: "INVALID_USERNAME"
        }
    },
    'gender': {
        notEmpty: true,
        matches: {
            options: ["male|female", "i"],
            errorMessage: "Gender is invalid"
        }
    },
    'phone': {
        notEmpty: true,
        matches: {
            options: ["^[0-9]+$", "i"],
            errorMessage: "PHONE_TYPE"
        }
    }
};

module.exports = registerSchema;