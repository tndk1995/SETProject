var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var loginSchema = new Schema({
    indentityKey: {
        type: String,
        required: true,
        unique: true
    },
    failedAttempts: {
        type: Number,
        required: true
    },
    timeOut: {
        type: Date,
        required: true,
        default: new Date()
    },
    inProgress: {
        type: Boolean
    }
});

var loginAuth = mongoose.model('loginAuth', loginSchema)
module.exports = loginAuth;