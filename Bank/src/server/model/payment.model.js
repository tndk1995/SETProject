var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var paymentSchema = new Schema({
    status: {
        type: Boolean
    },
    paymentId: {
        type: String
    },
    createAt: {
        type: Date
    },
    cardHolderId: {
        type: String
    },
    merchantId: {
        type: String
    }
});
var payment = mongoose.model('payment', paymentSchema);

module.exports = payment;