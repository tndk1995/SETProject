var loginSchema = {
    'email': {
        notEmpty: true,
        isEmail: {
            errorMessage: "INVALID_EMAIL"
        }
    },
    'password': {
        notEmpty: true,
        isLength: {
            options: [{ min: 6 }],
            errorMessage: "PASSWORD_LENGTH"
        },
        matches: {
            options: ["(?=.*[a-zA-Z])(?=.*[0-9]+).*", "g"],
            errorMessage: "PASSWORD_TYPE"
        }
    }
};

module.exports = loginSchema;