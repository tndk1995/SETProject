// var ursa = require('ursa');

// module.exports = generateKey;

// function generateKey() {
//     var keys = ursa.generatePrivateKey();
//     return {
//         privateKey: keys.toPrivatePem('base64'),
//         publicKey: keys.toPublicPem('base64')
//     }
// }

var forge = require('node-forge');

module.exports = generateKey;

function generateKey() {
    var pair = forge.pki.rsa.generateKeyPair(1024, 0x10001);
    return {
        publicKey: forge.pki.publicKeyToPem(pair.publicKey),
        privateKey: forge.pki.privateKeyToPem(pair.privateKey)
    }
}