/*jshint esversion: 6*/
var nodemailer = require('nodemailer');
var fs = require("fs");
var path = require("path");
var jwt = require('./jwt.util');
var transport = require('nodemailer-smtp-transport');
var transporter = nodemailer.createTransport(transport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
        user: 'biaiteam12@gmail.com',
        pass: 'Biaiteam12'
    }
}));
var rootPath = path.join(__dirname, "emailTemplate");
module.exports = {
    sendMailVerify: sendMailVerify,
    sendMailCert: sendMailCert,
    sendMailBankPubkey: sendMailBankPubkey,
    sendMailCompletePayment: sendMailCompletePayment,
    sendMailVerifyPayment: sendMailVerifyPayment
};;

function sendMailVerify(email, content) {

    var token = jwt.signToken({ email: email, role: 'user' }, "0");
    var htmlTemplate = fs.readFileSync(path.join(rootPath, "verify.html"), "utf8");

    htmlTemplate = htmlTemplate.replace("$verifyEmail", `https://localhost:8443/activateAccount/${token}`)
        .replace("$typeCard", content.typeCard)
        .replace("$bankId", content.bankId)
        .replace("$PIN", content.PIN);
    var mailOptions = {
        from: '"Bank" <biaiteam12@gmail.com>',
        to: email,
        subject: 'Verify your account',
        html: htmlTemplate,
        attachments: [{ // utf-8 string as an attachment
            filename: 'CardHorderPrivateKey.pem',
            content: content.privateKey
        }]
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });
}

function sendMailCert(email, publicKey) {
    var token = jwt.CASignCert({ email: email, publicKey: publicKey });
    var htmlTemplate = fs.readFileSync(path.join(rootPath, "verify-success.html"), "utf8");
    var mailOptions = {
        from: '"Bank" <biaiteam12@gmail.com>',
        to: email,
        subject: 'Bank sent your Certification',
        html: htmlTemplate,
        attachments: [{ // utf-8 string as an attachment
            filename: 'CardHorderCert.json',
            content: token
        }]
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });
}

function sendMailBankPubkey(email, bankPublicKey) {
    var token = jwt.CASignCert({ email: email, bankPublicKey: bankPublicKey });
    var htmlTemplate = fs.readFileSync(path.join(rootPath, "verify-success.html"), "utf8");
    var mailOptions = {
        from: '"Bank" <biaiteam12@gmail.com>',
        to: email,
        subject: 'Bank sent Public Key',
        html: htmlTemplate,
        attachments: [{ // utf-8 string as an attachment
            filename: 'BankPubKey.json',
            content: token
        }]
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });
}

function sendMailCompletePayment(merchantEmail, cardHorderEmail, paymentInfo) {
    var cardHolderHtmlTemplate = fs.readFileSync(path.join(rootPath, "billing.html"), "utf8");

    cardHolderHtmlTemplate = cardHolderHtmlTemplate.replace("$cardHolderEmail", cardHorderEmail)
        .replace(/totalCost/g, paymentInfo.totalCost)
        .replace(/cardHolderBalance/, paymentInfo.cardHolderBalance)
        .replace("$paymentId", paymentInfo.paymentId);
    var mailOptions = {
        from: '"Bank" <biaiteam12@gmail.com>',
        to: cardHorderEmail,
        subject: 'Bank sent Card Holder a Receipt',
        html: cardHolderHtmlTemplate
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });
    var merchantHtmlTemplate = fs.readFileSync(path.join(rootPath, "billingToMerchant.html"), "utf8");

    merchantHtmlTemplate = merchantHtmlTemplate.replace("$cardHolderEmail", cardHorderEmail)
        .replace(/totalCost/g, paymentInfo.totalCost)
        .replace(/merchantBalance/, paymentInfo.merchantBalance)
        .replace("$paymentId", paymentInfo.paymentId);
    var mailOptions = {
        from: '"Bank" <biaiteam12@gmail.com>',
        to: merchantEmail,
        subject: 'Bank sent Card Holder a Receipt',
        html: merchantHtmlTemplate
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });
}

function sendMailVerifyPayment(cardHorderEmail, paymentInfo) {
    var token = jwt.signToken({ email: cardHorderEmail, paymentId: paymentInfo.paymentId }, "0");
    var cardHolderHtmlTemplate = fs.readFileSync(path.join(rootPath, "billingVerify.html"), "utf8");

    cardHolderHtmlTemplate = cardHolderHtmlTemplate.replace("$cardHolderEmail", cardHorderEmail)
        .replace(/totalCost/g, paymentInfo.totalCost)
        .replace(/cardHolderBalance/, paymentInfo.cardHolderBalance)
        .replace("$paymentId", paymentInfo.paymentId)
        .replace("$verifyEmail", `https://localhost:8443/verifyPayment/${token}`);
    var mailOptions = {
        from: '"Bank" <biaiteam12@gmail.com>',
        to: cardHorderEmail,
        subject: 'Card Holder confirm a Payment',
        html: cardHolderHtmlTemplate
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });
}