'use strict';
var jwt = require('jsonwebtoken');
var config = require('../config/config');
module.exports = {
    signToken: signToken,
    CASignCert: CASignCert,
    signTokenWithExpire: signTokenWithExpire
};

function signToken(information, state) {
    if (state === "0") {
        console.log(state);
        var token = jwt.sign(information, config.certKey, { algorithm: 'RS256', expiresIn: '7d' });
    } else {
        var token = jwt.sign(information, config.certKey, { algorithm: 'RS256', expiresIn: '1y' });
    }
    return token;
}

function CASignCert(information) {
    return jwt.sign(information, config.CAcertKey, { algorithm: 'RS256', expiresIn: '1y' });
}

function signTokenWithExpire(information, expireTime) {
    return jwt.sign(information, config.certKey, { algorithm: 'RS256', expiresIn: expireTime });
}