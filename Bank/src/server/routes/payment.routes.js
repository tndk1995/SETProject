var router = require('express').Router();
var paymentDao = require("../dao/payment.dao");
var middlewarePaymentParser = require('../middlewares/payment-cert-parser');

module.exports = function() {
    router.post('/', middlewarePaymentParser.paymentParser(), payment);
    router.post('/request', middlewarePaymentParser.requestPaymentParser(), requestPayment);

    function payment(req, res, next) {
        var request = {
            payment: req.body.payment,
            orderInformation: req.body.orderInformation,
            encryptedOI: req.body.encryptedOI
        };

        paymentDao.payment(request)
            .then((response) => {
                res.status(200).send(response).end();
            })
            .catch((err) => {
                next(err);
            });
    }

    function requestPayment(req, res, next) {
        var request = {
            PIPacketCipher: req.body.PIPacketCipher,
            DigitalEnvelope: req.body.DigitalEnvelope,
            cardHolderCert: req.body.cardHolderCert,
            merchantCert: req.body.merchantCert,
            // orderInformation: req.body.orderInformation
        };

        paymentDao.requestPayment(request)
            .then((response) => {
                res.status(200).send(response).end();
            })
            .catch((err) => {
                next(err);
            });
    }
    return router;
};