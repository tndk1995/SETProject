angular.module('app.homepage')
    .controller('homepageController', homepageController);
homepageController.$inject = ['$state', 'homeService', 'authService', 'productService', 'cartService', '$mdSidenav', 'paymentService', '$mdStepper'];

function homepageController($state, homeService, authService, productService, cartService, $mdSidenav, paymentService, $mdStepper) {
    var vm = this;
    vm.isBanner;

    vm.logout = logout;
    vm.toggleRight = toggleRight;

    vm.getTotalPage = getTotalPage;
    vm.getAllProduct = getAllProduct;
    vm.checkAddedCart = checkAddedCart;

    vm.addCart = addCart;
    vm.showCart = showCart;
    vm.removeCart = removeCart;

    vm.startPayment = startPayment;
    vm.cancelPayment = cancelPayment;
    vm.completePayment = completePayment;
    vm.payment = payment;

    function toggleRight() {
        vm.state = 'login';
        $mdSidenav('right').toggle();
    }
    init();

    function init() {
        vm.isStartPayment = false;
        vm.isBanner = true;
        vm.totalPage = 0;
        vm.currentPage = 1;
        vm.category = '';
        if (authService.login(null, 2)) {
            homeService.getUser().then(
                (res) => {
                    vm.user = res.data.user;
                },
                (err) => {
                    toastr.error(err);
                });
            vm.myCart = cartService.getCart();
        }
        getAllProduct(1, vm.category);
    }

    function showCart() {
        vm.myCart = cartService.getCart();
        $('#myCart').modal('toggle');
    }

    function addCart(product) {
        if (!vm.user) {
            vm.toggleRight();
        } else if (product.amountToBuy > 0 && product.amountToBuy <= product.amount) {
            vm.myCart = cartService.addCart(product);
        }
    }

    function removeCart(product) {
        vm.myCart = cartService.removeCart(product);
    }

    function checkAddedCart(product) {
        if (vm.myCart) {
            return vm.myCart.products.filter(prod => {
                return prod._id === product._id;
            }).length;
        } else return false;
    }

    function getAllProduct(pageIndex, category) {
        vm.category = category;
        var request = {
            category: category
        }
        productService.getProduct(pageIndex, request)
            .then(function(res) {
                    vm.products = res.data.items;
                    vm.totalPage = res.data.totalPage;
                    vm.pageSize = res.data.pageSize;
                    vm.currentPage = res.data.currentPage;
                },
                function(err) {
                    toastr.error(err);
                }
            );

    }

    function getTotalPage(num) {
        return new Array(num);
    }

    function logout() {
        toastr.success(authService.logout());
        $state.reload();
    }

    //start payment
    function startPayment() {
        if (!vm.isStartPayment) {
            vm.isStartPayment = true;
        }
    }

    function cancelPayment() {
        vm.isStartPayment = false;
        vm.isLoading = false;
    }

    function completePayment() {
        vm.myCart = cartService.clearCart();
        $('#myCart').modal('hide');
        vm.isStartPayment = false;
        getAllProduct(1, vm.category);
    }
    async function payment(step) {
        var steppers = $mdStepper('stepper-payment');
        if (step == 1) {
            var request = {
                typeOfPayment: vm.typeOfPayment,
                address: vm.address,
                dateOfRequest: Date.now()
            }
            paymentService.requestPayment(request)
                .then(function(res) {
                        vm.responseFirstStep = res.payment;
                        steppers.next();
                    },
                    function(err) {
                        toastr.error(err);
                    }
                );
        } else if (step == 2) {
            var request = {
                orderInformation: {...vm.myCart, paymentId: vm.responseFirstStep._id },
                paymentInformation: {
                    paymentId: vm.responseFirstStep._id,
                    typeOfPayment: vm.responseFirstStep.typeOfPayment,
                    bankID: vm.bankID,
                    PIN: vm.PIN
                        // bankID: '59f3495097d1',
                        // PIN: 1927
                },
                privateKey: await readFile("privateKey"),
                cardHolderCert: await readFile("cardHolderCert"),
                responseFirstStep: vm.responseFirstStep
            }
            vm.isLoading = true;
            paymentService.completePayment(request)
                .then(function(res) {
                        vm.isLoading = false;
                        steppers.next();
                    },
                    function(err) {
                        toastr.error(err);
                        cancelPayment();
                    }
                );
        }
    }

    function readFile(id) {
        return new Promise((resolve, reject) => {
            var file = $("#" + id).prop('files')[0];
            var reader;
            if (file) {
                reader = new FileReader();
                reader.readAsText(file, "UTF-8");
                reader.onload = function(evt) {
                    resolve(evt.target.result);
                }
                reader.onerror = function(evt) {
                    toastr.error("error reading file");
                    reject();

                }
            }
        })
    }
}