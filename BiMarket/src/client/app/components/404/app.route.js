(function() {
    'use strict';

    angular
        .module('app')
        .run(appRun)
        .config(authConfig);

    function authConfig($stateProvider, $httpProvider) {
        $stateProvider
            .state('root', {
                url: '/',
            });
        $httpProvider.interceptors.push('tokenInterceptor');

    }

    appRun.$inject = ['$state', '$rootScope', 'authService', 'routerHelper', "jwtHelper"];

    function appRun($state, $rootScope, authService, routerHelper, jwtHelper) {

        var otherwise = '/404';

        routerHelper.configureStates(getStates(), otherwise);

        $rootScope.$on('$stateChangeStart', function(event, toState, fromState) {
            var token = authService.login(null, 2);
            if (token) {
                if (jwtHelper.isTokenExpired(token)) {
                    authService.logout();
                    event.preventDefault();
                    $state.go('homepage');
                }
            }
            if (toState.url === '/store') {
                if (!token) {
                    event.preventDefault();
                    $state.go('homepage');
                }
                // } else if (toState.url === '/homepage') {
                //     if (!authService.login(null, 2)) {
                //         event.preventDefault();
                //         $state.go('auth.login');
                //     }
            } else if (toState.url === '/') {
                event.preventDefault();
                $state.go('homepage');
            }

        });
    }

    function getStates() {
        return [{
            state: '404',
            config: {
                url: '/404',
                templateUrl: 'app/components/404/404.html',
                title: '404'
            }
        }];
    }
})();