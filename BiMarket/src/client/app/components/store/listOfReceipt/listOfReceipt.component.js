angular.module('app.store')
    .component('listOfReceipt', {
        bindToController: true,
        templateUrl: 'app/components/store/listOfReceipt/listOfReceipt.html',
        controller: listOfReceiptController,
        controllerAs: 'vm',
        bindings: {
            user: '=',
            namePage: '=',
            role: '='
        }
    });
listOfReceiptController.$inject = ['$state', 'paymentService'];

function listOfReceiptController($state, paymentService) {
    var vm = this;
    vm.getAllReceipt = getAllReceipt;
    vm.totalPage = 0;
    vm.getTotalPage = getTotalPage;
    vm.getAllReceipt(1);
    vm.viewDetail = viewDetail;

    function getAllReceipt(pageIndex) {
        paymentService.getAllReceipt(pageIndex)
            .then(function(res) {
                    vm.receipts = res.data.items;
                    vm.totalPage = res.data.totalPage;
                    vm.pageSize = res.data.pageSize;
                    vm.currentPage = res.data.currentPage;
                },
                function(err) {
                    toastr.error(err);

                }
            );
    }


    function getTotalPage(num) {
        return new Array(num);
    }

    function viewDetail(receipt) {
        vm.rpSelected = receipt;
    }
}