angular.module('app.store')
    .component('editAccount', {
        bindings: {
            user: '=',
            namePage: '=',
            role: '='
        },
        templateUrl: 'app/components/store/edit-account/edit-account.html',
        controller: editAccountController,
        controllerAs: 'vm'

    });

editAccountController.$inject = ['$scope', 'homeService', '$state'];

function editAccountController($scope, homeService, $state) {
    var vm = this;
    vm.changePass = false;
    vm.edit = edit;
    vm.cancel = cancel;

    function edit() {
        var update = {
            _id: vm.user._id,
            username: vm.username || vm.user.username,
            phone: vm.phone || vm.user.phone,
            gender: vm.user.gender,
            birthday: vm.birthday || vm.user.birthday
        };
        if (vm.changePass) {
            update.oldPass = vm.oldPassword;
            update.newPass = vm.password;
        }
        return homeService.updateUser(update).then(
            function(res) {
                toastr.success(res);
                $state.reload();
            },
            function(err) {
                toastr.error(err);
            }
        );
    }

    function cancel() {
        $state.reload();
    }

}