angular.module('app.store')
    .component('editProduct', {
        bindings: {
            product: '=',
            namePage: '='
        },
        templateUrl: 'app/components/store/edit-store/edit-product/edit-product.html',
        controller: editProductController,
        controllerAs: 'vm'

    });

editAccountController.$inject = ['$scope', 'productService', '$state'];

function editProductController($scope, productService, $state) {
    var vm = this;
    vm.edit = edit;
    vm.cancel = cancel;

    function edit() {
        var request = {
            id: vm.product._id,
            name: vm.productName,
            cost: vm.productCost,
            amount: vm.productAmount,
            category: vm.product.category,
            files: vm.productImg
        }
        productService.updateProduct(request).then(
            function(res) {
                toastr.success(res.data.message);
                $state.reload();
            },
            function(err) {
                toastr.error(err);
            }
        );
    }

    function cancel() {
        $state.reload();
    }

}