angular.module('app.store')
    .component('editStore', {
        bindToController: true,
        templateUrl: 'app/components/store/edit-store/edit-store.html',
        controller: editStoreController,
        controllerAs: 'vm',
        bindings: {
            user: '=',
            productEdit: '=',
            namePage: '='
        }
    });
editStoreController.$inject = ['$scope', '$state', '$mdDialog', 'productService'];

function editStoreController($scope, $state, $mdDialog, productService) {
    var vm = this;
    vm.getAllProduct = getAllProduct;
    vm.createProduct = createProduct;
    vm.getTotalPage = getTotalPage;
    vm.editProduct = editProduct;
    vm.disableProduct = disableProduct;
    vm.reload = reload;
    init()

    function init() {
        vm.totalPage = 0;
        vm.currentPage = 1;
        getAllProduct(1);
    }

    function getAllProduct(pageIndex) {
        productService.getProductByMerchant(pageIndex)
            .then(function(res) {
                    vm.products = res.data.items;
                    vm.totalPage = res.data.totalPage;
                    vm.pageSize = res.data.pageSize;
                    vm.currentPage = res.data.currentPage;
                },
                function(err) {
                    toastr.error(err);
                }
            );
    }

    function disableProduct(id) {
        productService.disableProduct(id)
            .then(function(res) {
                    toastr.success(res.data.message);
                    reload();
                },
                function(err) {
                    toastr.error(err);
                }
            );
    }

    function getTotalPage(num) {
        return new Array(num);
    }

    function createProduct() {
        var request = {
            name: vm.productName,
            cost: vm.productCost,
            amount: vm.productAmount,
            category: vm.productCategory,
            files: vm.productImg
        }

        productService.createProduct(request).then(
            function(res) {
                toastr.success(res.data.message);
                reload();
            },
            function(err) {
                toastr.error(err);
            }
        );
    }

    function editProduct(product) {
        vm.productEdit = product;
        vm.namePage = 5;
    }

    function reload() {
        vm.productName = "";
        vm.productCost = "";
        vm.productAmount = "";
        vm.productCategory = "";
        vm.productImg = "";
        vm.productID = "";
        $('#productModal').modal('hide');
        init();
    }
}