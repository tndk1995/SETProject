angular.module('app.store')
    .config(storeConfig);

function storeConfig($stateProvider) {
    $stateProvider
        .state('store', {
            url: '/store',
            templateUrl: 'app/components/store/store.html',
            controller: 'storeController',
            controllerAs: 'vm'
        });
}