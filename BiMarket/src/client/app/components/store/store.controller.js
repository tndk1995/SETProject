angular.module('app.store')
    .controller('storeController', storeController);

storeController.$inject = ['$state', 'homeService', 'authService', 'productService']

function storeController($state, homeService, authService, productService) {
    var vm = this;
    vm.namePage = 0;
    vm.logout = logout;
    init();

    function init() {
        if (vm.namePage == 0) {
            homeService.getUser().then(
                function(res) {
                    vm.user = res.data.user;
                    vm.userEdit = vm.user;
                    if (vm.user.role == 'user') {
                        vm.namePage = 2;
                    } else {
                        vm.namePage = 1;
                    }
                },
                function(err) {
                    toastr.error(err);
                });

        }

    }

    function logout() {
        toastr.success(authService.logout());
        $state.go('homepage');
    }


}