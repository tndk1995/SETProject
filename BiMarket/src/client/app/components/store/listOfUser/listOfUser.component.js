angular.module('app.store')
    .component('listOfUser', {
        bindToController: true,
        templateUrl: 'app/components/store/listOfUser/listOfUser.html',
        controller: listOfUserController,
        controllerAs: 'vm',
        bindings: {
            userEdit: '=',
            namePage: '=',
            role: '='
        }
    });
listOfUserController.$inject = ['$state', 'homeService'];

function listOfUserController($state, homeService) {
    var vm = this;
    vm.deleteUser = deleteUser;
    vm.editUser = editUser;
    vm.getAllUser = getAllUser;
    vm.totalPage = 0;
    vm.getTotalPage = getTotalPage;
    vm.getAllUser(1);

    function getAllUser(pageIndex) {
        homeService.getAllUser(pageIndex)
            .then(function(res) {
                    vm.users = res.data.items;
                    vm.totalPage = res.data.totalPage;
                    vm.currentPage = res.data.currentPage;
                },
                function(err) {
                    toastr.error(err);

                }
            );
    }

    function deleteUser(id) {
        homeService.deleteUser(id)
            .then(function(res) {
                    toastr.success(res);
                    getAllUser();
                },
                function(err) {
                    toastr.error(err);

                }
            );
    }

    function editUser(user) {
        vm.userEdit = user;
        vm.namePage = 2;
    }

    function getTotalPage(num) {
        return new Array(num);
    }

}