angular.module('app.store')
    .component('settingServer', {
        bindings: {
            user: '=',
            namePage: '='
        },
        templateUrl: 'app/components/store/setting-server/setting-server.html',
        controller: settingServerController,
        controllerAs: 'vm'

    });

settingServerController.$inject = ['$scope', 'homeService', '$state'];

function settingServerController($scope, homeService, $state) {
    var vm = this;
    vm.upload = upload;
    vm.cancel = cancel;

    async function upload() {
        var setting = {
            email: vm.email,
            merchantPrivateKey: await readFile("merchantPrivateKey"),
            merchantCert: await readFile("merchantCert"),
            bankCert: await readFile("bankCert"),
        };
        return homeService.settingServer(setting).then(
            function(res) {
                toastr.success(res);
                $state.reload();
            },
            function(err) {
                toastr.error(err);
            }
        );
    }

    function readFile(id) {
        return new Promise((resolve, reject) => {
            var file = $("#" + id).prop('files')[0];
            var reader;
            if (file) {
                reader = new FileReader();
                reader.readAsText(file, "UTF-8");
                reader.onload = function(evt) {
                    resolve(evt.target.result);
                }
                reader.onerror = function(evt) {
                    toastr.error("error reading file");
                    reject();

                }
            }
        })
    }

    function cancel() {
        $state.reload();
    }

}