'use strict';
angular.module('app')
    .component('auth', {
        bindings: {
            state: '='
        },
        templateUrl: 'app/shared/directives/auth/auth.html',
        controller: authController,
        controllerAs: 'vm'
    });

authController.$inject = ['$scope', '$state', 'authService'];

function authController($scope, $state, authService) {
    var vm = this;
    vm.login = login;
    vm.gender = 'Male';
    vm.register = register;

    function register() {
        if (vm.password !== vm.password_confirm) {
            toastr.error("Password confirm not match");
        } else {
            var newUser = {
                email: vm.email,
                password: vm.password,
                username: vm.username,
                gender: vm.gender,
                phone: vm.phone,
                birthday: vm.birthday
            };
            return authService.register(newUser).then(
                function(res) {
                    toastr.success(res);
                    vm.state = 'login';
                },
                function(err) {
                    toastr.error(err);
                }
            );
        }
    };

    function login() {
        var request = {
            email: vm.email,
            password: vm.password
        };

        return authService.login(request, vm.remember === true ? 1 : 0).then(function(res) {
            toastr.success(res);
            $state.reload();
        }, function(err) {
            toastr.error(err);
        });
    }
}