angular.module('services.product', ['services.errorTranslator', 'services.auth', 'ngFileUpload'])
    .factory('productService', ['$q', '$http', 'errTransService', 'Upload', productService]);

function productService($q, $http, errTransService, Upload) {
    return {
        getProduct: getProduct,
        getProductByMerchant: getProductByMerchant,
        updateProduct: updateProduct,
        createProduct: createProduct,
        disableProduct: disableProduct
    }

    function getProduct(pageIndex, request) {
        var deferred = $q.defer();
        $http.post('api/products/get/' + pageIndex, request)
            .then(function(res) {
                deferred.resolve(res);
            }, function(err) {
                deferred.reject(errTransService[err.data.message]);
            });

        return deferred.promise;
    }

    function disableProduct(id) {
        var deferred = $q.defer();
        $http.post('api/products/disable/' + id)
            .then(function(res) {
                deferred.resolve(res);
            }, function(err) {
                deferred.reject(errTransService[err.data.message]);
            });

        return deferred.promise;
    }

    function getProductByMerchant(pageIndex) {
        var deferred = $q.defer();
        $http.get('api/products/merchant/' + pageIndex)
            .then(function(res) {
                deferred.resolve(res);
            }, function(err) {
                deferred.reject(errTransService[err.data.message]);
            });

        return deferred.promise;
    }

    function createProduct(request) {
        var deferred = $q.defer();
        Upload.upload({
            url: 'api/products',
            data: {
                name: request.name,
                cost: request.cost,
                amount: request.amount,
                category: request.category,
                file: request.files
            }
        }).then(function(resp) {
            deferred.resolve(resp);
        }, function(err) {
            deferred.reject(err.data.message);
        });

        return deferred.promise;
    }

    function updateProduct(request) {
        var deferred = $q.defer();
        Upload.upload({
            url: 'api/products/update',
            data: {
                _id: request.id,
                name: request.name,
                cost: request.cost,
                amount: request.amount,
                category: request.category,
                file: request.files
            }
        }).then(function(resp) {
            deferred.resolve(resp);
        }, function(err) {
            deferred.reject(err.data.message);
        });

        return deferred.promise;
    }
}