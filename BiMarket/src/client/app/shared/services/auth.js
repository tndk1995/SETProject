(function() {

    angular.module('services.auth', ['ngStorage', 'services.errorTranslator'])
        .factory('authService', ['$q', '$http', '$localStorage', '$sessionStorage', 'jwtHelper', 'errTransService', authService]);

    function authService($q, $http, $localStorage, $sessionStorage, jwtHelper, errTransService) {

        var storage;

        function login(request, state) {
            var deferred = $q.defer();

            if (state === 1) {
                storage = $localStorage;
            } else if (state === 2) {
                if ($localStorage.token) {
                    return getToken();
                }
                return false;
            }

            $http.post('api/auth/signin/' + state, request)
                .then(function(res) {
                    storage.token = res.data.token;
                    deferred.resolve('Login successful');
                }, function(err) {
                    deferred.reject(errTransService[err.data.message]);
                });

            return deferred.promise;
        }

        function register(newUser) {
            var deferred = $q.defer();
            if (newUser) {
                $http.post('api/auth/signup', newUser)
                    .then(function(res) {
                        deferred.resolve('Register successful');
                    }, function(err) {
                        deferred.reject(errTransService[err.data.message]);
                    });
            }
            return deferred.promise;
        }

        function logout() {
            delete $localStorage.token;
            delete $localStorage.cart;
            return 'Logout successful';
        }

        function sendMail(email) {
            var deferred = $q.defer();
            if (email) {
                $http.post('api/auth/sendEmail', email)
                    .then(function(res) {
                        deferred.resolve('Sent');
                    }, function(err) {
                        deferred.reject(errTransService[err.data.message]);
                    });
            }
            return deferred.promise;
        }

        function resetPassword(request) {
            var deferred = $q.defer();
            if (request) {
                $http.post('api/auth/resetPassword', request)
                    .then(function(res) {
                        deferred.resolve('Changed successfully');
                    }, function(err) {
                        deferred.reject(errTransService[err.data.message]);
                    });
            }
            return deferred.promise;
        }

        function getDecodedToken() {
            if ($localStorage.token) {
                return jwtHelper.decodeToken($localStorage.token);

            } else if ($sessionStorage.token) {
                return jwtHelper.decodeToken($sessionStorage.token);

            }
            return false;
        }

        function getToken() {
            return $localStorage.token;;
        }

        return {
            login: login,
            logout: logout,
            register: register,
            storage: storage,
            sendMail: sendMail,
            resetPassword: resetPassword,
            getToken: getToken,
            getDecodedToken: getDecodedToken
        };

    }
})();