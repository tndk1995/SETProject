angular.module('services.cart', ['ngStorage', 'services.errorTranslator', 'services.auth'])
    .factory('cartService', ['$q', '$http', '$localStorage', 'errTransService', cartService]);

function cartService($q, $http, $localStorage, errTransService) {
    return {
        getCart: getCart,
        removeCart: removeCart,
        addCart: addCart,
        clearCart: clearCart
    }

    function getCart() {
        return $localStorage.cart || createCart();
    }


    function addCart(product) {
        var cart = $localStorage.cart || createCart();
        cart.products.push(product);
        cart.totalCost += product.cost * product.amountToBuy;
        $localStorage.cart = cart;
        return cart;
        toastr.success("Added to cart");
    }

    function removeCart(product) {
        var cart = $localStorage.cart || createCart();
        cart.products = cart.products.filter(pro => {
            return pro._id !== product._id;
        });
        cart.totalCost -= product.cost * product.amountToBuy;
        $localStorage.cart = cart;
        return cart;
    }

    function clearCart() {
        delete $localStorage.cart;
        return;
    }

    function createCart() {
        return {
            products: [],
            totalCost: 0
        }
    }
}