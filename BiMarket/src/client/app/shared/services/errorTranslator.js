(function() {

    'use strict';

    angular.module('services.errorTranslator', [])
        .factory('errTransService', function() {
            var errorTranslator = {
                'ERROR_INPUT': 'Invalid input fields',
                'SYSTEM_ERROR': 'Please try again',
                'USER_NOT_FOUND': 'Wrong email or password',
                'PASSWORD_INCORRECT': 'Wrong email or password',
                'NO_CAPTCHA': 'Please select captcha',
                'FAIL_CAPTCHA': 'Failed captcha verification',
                'OVER_TIME': 'Failed 5 times. Try again in 1 minute',
                'IN_PROGRESS': 'Work in progress! Please try again',
                'UN_AUTHORIZED': 'Failed to authenticate token.',
                'NO_TOKEN': '[Token Error]',
                'PASSWORD_OLD_NOT_CORRECT': 'Wrong password! Please try again',
                'TOKEN_EXPIRED': "Your token is expired! Please Sign in again",
                'INVALID_EMAIL': "Invalid Email",
                'PASSWORD_LENGTH': "Must be at least 6 characters",
                'PASSWORD_TYPE': "Password must be alphanumeric.",
                'USERNAME_LENGTH': "Username must be under 25 characters",
                'INVALID_USERNAME': "Username has invalid characters",
                'PHONE_TYPE': "Phone must be number"
            };
            return errorTranslator;
        });
})();