angular.module('services.payment', ['services.errorTranslator', 'services.auth', 'ngFileUpload'])
    .factory('paymentService', ['$q', '$http', 'errTransService', 'Upload', 'jwtHelper', paymentService]);

function paymentService($q, $http, errTransService, Upload, jwtHelper) {
    return {
        requestPayment: requestPayment,
        completePayment: completePayment,
        getAllReceipt: getAllReceipt
    }

    function getAllReceipt(pageIndex) {
        var deferred = $q.defer();
        $http.get('api/payment/' + pageIndex)
            .then(function(res) {
                deferred.resolve(res);
            }, function(err) {
                deferred.reject(errTransService[err.data.message]);
            });

        return deferred.promise;
    }

    function requestPayment(request) {
        var deferred = $q.defer();

        $http.post('api/payment', request)
            .then(function(res) {
                deferred.resolve(res.data);
            }, function(err) {
                deferred.reject(err.data.message);
            });

        return deferred.promise;
    }

    function completePayment(request) {
        var privateKey = forge.pki.privateKeyFromPem(request.privateKey);
        var OIMD = CryptoJS.SHA1(JSON.stringify(request.orderInformation)).toString();
        var PIMD = CryptoJS.SHA1(JSON.stringify(request.paymentInformation)).toString();
        var POMD = CryptoJS.SHA1(OIMD + PIMD);

        var md = forge.md.sha1.create();
        md.update(POMD);
        var DualSignature = privateKey.sign(md);
        var tempKey = CryptoJS.lib.WordArray.random(128 / 8).toString();
        var PIPacket = {
            paymentInformation: request.paymentInformation,
            DualSignature: forge.util.encode64(DualSignature),
            OIMD: OIMD
        };

        var PIPacketCipher = CryptoJS.AES.encrypt(JSON.stringify(PIPacket), tempKey);
        var publicKeyBank = forge.pki.publicKeyFromPem(jwtHelper.decodeToken(request.responseFirstStep.bankPubKeyToken).bankPublicKey);
        var DigitalEnvelope = publicKeyBank.encrypt(tempKey);

        var requestMessage = {
            PIPacketCipher: PIPacketCipher.toString(),
            DigitalEnvelope: forge.util.encode64(DigitalEnvelope),
            PIMD: PIMD,
            orderInformation: JSON.stringify(request.orderInformation),
            DualSignature: forge.util.encode64(DualSignature),
            cardHolderCert: request.cardHolderCert,
            paymentToken: request.responseFirstStep.paymentToken
        };
        // var publicKeyMerchant = forge.pki.publicKeyFromPem(jwtHelper.decodeToken(request.responseFirstStep.merchantPubKeyToken).publicKey);
        // var requestMessageEncrypted = publicKeyMerchant.encrypt(JSON.stringify(requestMessage), 'RSA-OAEP');
        // console.log(forge.util.encode64(requestMessageEncrypted));
        console.log("Request Message", requestMessage);

        var deferred = $q.defer();

        $http.post('api/payment/complete', requestMessage)
            .then(function(res) {
                deferred.resolve(res.data);
            }, function(err) {
                console.log(err)
                deferred.reject(err.data.message);
            });

        return deferred.promise;
    }

    // var signature = forge.util.decode64(signature64);
    // var publicKey = forge.pki.publicKeyFromPem(`-----BEGIN PUBLIC KEY-----
    // MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCgFcZ06e/wSUY8HaB7o7U4EKnK
    // 9i4h6d/SWY5DfXjXgp6xReyqNNJP2gYB0OjtZT4NRqBczSmJKG4afnbfnVDELv6y
    // V40PZMXfYSFhMo5c5lklcGBvo89YtnPg5bH2G0DoeyYC9mF4QUCR4Q2FU/dzZ3OP
    // JdZwcXpca0ggg/TU1wIDAQAB
    // -----END PUBLIC KEY-----`);
    // var md2 = forge.md.sha1.create();
    // md2.update(POMD);
    // var verified = publicKey.verify(md2.digest().bytes(), DualSignature);
    // console.log("verified", verified);

    // var rsa = forge.pki.rsa;
    // rsa.generateKeyPair({ bits: 2048, workers: 2 }, function (err, keypair) {
    //     // keypair.privateKey, keypair.publicKey
    //     console.log("private", keypair.privateKey);
    //     console.log("publicKey", keypair.publicKey);
    // });
    // var pair = forge.pki.rsa.generateKeyPair(1024, 0x10001);

    // // pair.privateKey has n, e, d, p, q, dP, qQ, qInv
    // // pair.publicKey has n, e
    // // Note that the private key contains the components of
    // // the public key.

    // // public key as PEM (typical storage)
    // console.log(forge.pki.publicKeyToPem(pair.publicKey));

    // // private key as PEM (typical unprotected storage)
    // console.log(forge.pki.privateKeyToPem(pair.privateKey));


    // function disableProduct(id) {
    //     var deferred = $q.defer();
    //     $http.post('api/products/disable/' + id)
    //         .then(function(res) {
    //             deferred.resolve(res);
    //         }, function(err) {
    //             deferred.reject(errTransService[err.data.message]);
    //         });

    //     return deferred.promise;
    // }

    // function getProductByMerchant(pageIndex) {
    //     var deferred = $q.defer();
    //     $http.get('api/products/merchant/' + pageIndex)
    //         .then(function(res) {
    //             deferred.resolve(res);
    //         }, function(err) {
    //             deferred.reject(errTransService[err.data.message]);
    //         });

    //     return deferred.promise;
    // }

    // function createProduct(request) {
    //     var deferred = $q.defer();
    //     Upload.upload({
    //         url: 'api/products',
    //         data: {
    //             name: request.name,
    //             cost: request.cost,
    //             amount: request.amount,
    //             category: request.category,
    //             file: request.files
    //         }
    //     }).then(function(resp) {
    //         console.log(resp);
    //         deferred.resolve(resp);
    //     }, function(err) {
    //         deferred.reject(err.data.message);
    //     });

    //     return deferred.promise;
    // }

    // function updateProduct(request) {
    //     var deferred = $q.defer();
    //     Upload.upload({
    //         url: 'api/products/update',
    //         data: {
    //             _id: request.id,
    //             name: request.name,
    //             cost: request.cost,
    //             amount: request.amount,
    //             category: request.category,
    //             file: request.files
    //         }
    //     }).then(function(resp) {
    //         console.log(resp);
    //         deferred.resolve(resp);
    //     }, function(err) {
    //         deferred.reject(err.data.message);
    //     });

    //     return deferred.promise;
    // }
}