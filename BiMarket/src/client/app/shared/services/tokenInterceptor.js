angular.module('tokenInterceptor.service', []).factory('tokenInterceptor', tokenInterceptor)

tokenInterceptor.$inject = ['$localStorage', '$sessionStorage'];

function tokenInterceptor($localStorage, $sessionStorage) {
    return {
        request: function(config) {
            var token;
            if ($localStorage.token) {
                token = $localStorage.token;

            }
            if (token) {
                config.headers['x-access-token'] = token;
            }
            return config;
        }

    }
};