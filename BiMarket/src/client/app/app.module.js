(function() {
    'use strict';

    angular.module('app', [
        'app.auth',
        'app.homepage',
        'app.store',

        'tokenInterceptor.service',
        'services.auth',
        'services.product',
        'services.cart',
        'services.payment',

        'ui.router',
        'angular-jwt',
        'ngStorage',
        'ngAnimate',
        'ngSanitize',
        'ngplus',
        'ngMaterial',
        'mdSteppers',
        'blocks.exception',
        'blocks.logger',
        'blocks.router'
    ]);

})();