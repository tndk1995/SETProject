/*jshint esversion: 6*/
var User = require('./../model/user.model');
var Product = require('./../model/product.model');
var successMessage = require('../services/successMessage');
var failMessage = require('../services/failMessage');
var jwtDecode = require('jwt-decode');
var pagination = require('../services/pagination');

module.exports = {
    createProduct: createProduct,
    updateProduct: updateProduct,
    getAllProduct: getAllProduct,
    disableProduct: disableProduct
};

function createProduct(request) {
    var newProduct = new Product({
        name: request.name,
        cost: request.cost,
        amount: request.amount,
        category: request.category,
        uploadType: request.uploadType,
        createDate: Date.now(),
        status: "available"
    });

    return newProduct.save()
        .then((response) => {
            return Promise.resolve({
                message: successMessage.product.create,
                product: response
            });

        }).catch((err) => {
            return Promise.reject({
                statusCode: 500,
                message: failMessage.product.systemErr
            });
        });

}

function updateProduct(request) {
    return Product.findById(request._id)
        .then((product) => {
            if (!product) {
                return Promise.reject({
                    statusCode: 404,
                    message: failMessage.product.notFound
                });
            }
            product.name = request.name || product.name;
            product.cost = request.cost || product.cost;
            product.amount = request.amount || product.amount;
            product.category = request.category || product.category;
            product.uploadType = request.uploadType || product.uploadType;

            return product.save()
                .then((res) => {
                    console.log(res);
                    return Promise.resolve({
                        product: res,
                        message: successMessage.product.update
                    });
                })
                .catch((err) => {
                    console.log(err);
                    return Promise.reject({
                        message: failMessage.product.systemErr
                    });
                });
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}

function disableProduct(request) {
    return Product.findById(request._id)
        .then((product) => {
            if (!product) {
                return Promise.reject({
                    statusCode: 404,
                    message: failMessage.product.notFound
                });
            }
            if (product.status == 'available') {
                product.status = 'unavailable';
            } else {
                product.status = 'available';
            }
            return product.save()
                .then((res) => {
                    return Promise.resolve({
                        product: res,
                        message: successMessage.product.update
                    });
                })
                .catch((err) => {
                    console.log(err);
                    return Promise.reject({
                        message: failMessage.product.systemErr
                    });
                });
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}

function getAllProduct(request) {
    return Product.count(request.query).then(function(count) {
        return Product.find(request.query)
            .skip((request.pageIndex > 0) ? (request.pageIndex - 1) * request.pageSize : 0)
            .limit(request.pageSize)
            .exec().then(function(products) {
                var res = pagination.pagination(products, count, request.pageIndex, request.pageSize);
                return Promise.resolve(res);
            })
            .catch((err) => {
                return Promise.reject(err);
            });
    });
}