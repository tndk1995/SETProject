/*jshint esversion: 6*/
var User = require('./../model/user.model');
var cryptoPasswordUtil = require('./../utils/crypto');
var jwt = require('./../utils/jwt.util');
var successMessage = require('./../services/successMessage');
var failMessage = require('./../services/failMessage');
var userDao = require('.././dao/user.dao');
var emailValidate = require('./../utils/emailValidate');
var jwtDecode = require('jwt-decode');
var loginAuth = require('./../utils/loginAuth');

module.exports = {
    signin: signin,
    signup: signup,
    resetPassword: resetPassword,
    sendEmail: sendEmail
};

function signin(request, ip) {
    var key = request.email + ip;
    return loginAuth.inProgress(key)
        .then((res) => {
            if (res) {
                return Promise.reject({
                    message: failMessage.user.login.inProgress
                });
            }
            return loginAuth.canAuthenticate(key)
                .then((response) => {
                    if (!response) {
                        return Promise.reject({
                            message: failMessage.user.login.over5Time
                        });
                    }
                    return User.findOne({ email: request.email })
                        .then((user) => {
                            if (!user) {
                                return Promise.reject({
                                    statusCode: 404,
                                    message: failMessage.user.login.notFound
                                });
                            }

                            if (cryptoPasswordUtil.verifyPassword(user.password, user.salt, request.password)) {
                                var token = jwt.signToken(userDao.convertUserModelToUserResponse(user), request.state);
                                return loginAuth.successfulLoginAttempt(key).then(() => {
                                    return Promise.resolve({
                                        message: successMessage.user.login,
                                        'token': token
                                    });
                                });
                            }

                            return loginAuth.failedLoginAttempt(key)
                                .then(() => {
                                    return Promise.reject({
                                        statusCode: 400,
                                        message: failMessage.user.login.inCorrect
                                    });
                                });
                        })
                        .catch((err) => {
                            return Promise.reject(err);
                        });
                }).catch((err) => {
                    return Promise.reject(err);
                });
        }).catch((e) => {
            return Promise.reject(e);
        });
}

function signup(request) {
    return userDao.createUser(request)
        .then((response) => {
            return Promise.resolve({
                message: successMessage.user.register
                    // user: convertUserModelToUserResponse(response)
            });
        })
        .catch((err) => {
            return Promise.reject({
                message: failMessage.user.register.input
            });
        });
}

function sendEmail(request) {
    return User.findOne({ email: request.email })
        .then((email) => {
            if (!email) {
                return Promise.reject({
                    statusCode: 404,
                    message: failMessage.user.login.notFound
                });
            }

            var req = {
                email: request.email
            };
            var token = jwt.signToken(req);
            emailValidate.sendMail(request.email, token);
            return Promise.resolve({
                message: successMessage.user.sendEmail
            });
        })
        .catch(() => {
            return Promise.reject({
                message: failMessage.user.login.systemErr
            });
        });
}

function resetPassword(request) {
    var decoded = jwtDecode(request.token);

    return User.findOne({ email: decoded.email }).exec()
        .then((user) => {
            if (!user) {
                return Promise.reject({
                    statusCode: 404,
                    message: failMessage.user.login.notFound
                });
            }

            var newPassword = cryptoPasswordUtil.cryptoPassword(request.newPassword);
            user.password = newPassword.hash;
            user.salt = newPassword.salt;
            //console.log(passwordNew);
            return user.save()
                .then(() => {
                    return Promise.resolve({
                        message: successMessage.user.changePassword
                    });
                })
                .catch((err) => {
                    return Promise.reject(err);
                });
        })
        .catch(() => {
            return Promise.reject({
                message: failMessage.user.login.systemErr
            });
        });
}