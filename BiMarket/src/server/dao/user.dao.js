/*jshint esversion: 6*/
var User = require('./../model/user.model');
var cryptoUtil = require('./../utils/crypto');
var successMessage = require('../services/successMessage');
var failMessage = require('../services/failMessage');
var jwtDecode = require('jwt-decode');
var pagination = require('../services/pagination');
var emailPrivateKey = require('../utils/emailPrivateKey');
module.exports = {
    createUser: createUser,
    changePasswordUser: changePasswordUser,
    updateUser: updateUser,
    convertUserModelToUserResponse: convertUserModelToUserResponse,
    getUserByEmail: getUserByEmail,
    getAllUser: getAllUser,
    deleteUser: deleteUser,
    settingMerchant: settingMerchant
};

function convertUserModelToUserResponse(userModel) {
    var userObj = userModel.toObject();
    delete userObj.password;
    delete userObj.salt;
    return userObj;
}

function createUser(request) {
    return User.findOne({ email: request.email }).then((user) => {
            if (user) {
                return Promise.reject({
                    statusCode: 400,
                    message: failMessage.user.signup.duplicateUser
                });
            }
            var password = cryptoUtil.cryptoPassword(request.password);
            var newUser = new User({
                email: request.email,
                password: password.hash,
                salt: password.salt,
                username: request.username,
                gender: request.gender,
                phone: request.phone,
                role: request.role,
                birthday: request.birthday
            });

            return newUser.save()
                .then((response) => {
                    // emailPrivateKey.sendMail(newUser.email, key.privateKey);
                    return Promise.resolve({
                        message: successMessage.user.signup,
                        user: convertUserModelToUserResponse(response)
                    });

                }).catch(() => {
                    return Promise.reject({
                        statusCode: 500,
                        message: failMessage.user.signup.systemErr
                    });
                });
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}

function changePasswordUser(request) {

    var token = request.token;
    var decoded = jwtDecode(token);
    var _id = decoded._id;
    //console.log(decoded);
    return User.findById({
            _id: _id
        })
        .then((user) => {
            if (user) {
                //console.log("user")
                if (cryptoUtil.verifyPassword(user.password, user.salt, request.passwordOld)) {
                    var passwordNew = cryptoUtil.cryptoPassword(request.passwordNew);
                    user.password = passwordNew.hash;
                    user.salt = passwordNew.salt;
                    //console.log(passwordNew);
                    return user.save()
                        .then((res) => {
                            return Promise.resolve({
                                message: successMessage.user.changePassword
                            });
                        })
                        .catch((err) => {
                            return Promise.reject({
                                message: failMessage.user.changePassword.systemErr
                            });
                        });
                } else {
                    return Promise.reject({
                        message: failMessage.user.changePassword.passwordOldNotCorrect
                    });
                }
            }
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}

function updateUser(request) {
    var role = request.decoded.role;
    return User.findById({
            _id: request._id
        })
        .then((user) => {
            // console.log(user);
            if (!user) {
                return Promise.reject({
                    statusCode: 404,
                    message: failMessage.user.login.notFound
                });
            }
            if (request.oldPass) {
                if (cryptoUtil.verifyPassword(user.password, user.salt, request.oldPass)) {
                    var newPass = cryptoUtil.cryptoPassword(request.newPass);
                    user.password = newPass.hash;
                    user.salt = newPass.salt;
                } else {
                    return Promise.reject({
                        statusCode: 403,
                        message: failMessage.user.changePassword.passwordOldNotCorrect
                    });
                }
            }
            if (request.newPass && role === 'admin') {
                var newPass = cryptoUtil.cryptoPassword(request.newPass);
                user.password = newPass.hash;
                user.salt = newPass.salt;
            }
            user.username = request.username || user.username;
            user.birthday = request.birthday || user.birthday;
            user.gender = request.gender || user.gender;
            user.phone = request.phone || user.phone;
            return user.save()
                .then((res) => {
                    return Promise.resolve({
                        message: successMessage.user.update
                    });
                })
                .catch((err) => {
                    console.log(err);
                    return Promise.reject({
                        message: failMessage.user.changePassword.systemErr
                    });
                });

        })
        .catch((err) => {
            console.log(err)

            return Promise.reject(err);
        });
}

function settingMerchant(request) {
    return User.findOneAndUpdate({
        _id: request._id
    }, { $set: { merchantEmail: request.email } }, {
        new: true
    }).then(user => {
        return Promise.resolve(user);
    }).catch((err) => {
        console.log(err)
        return Promise.reject(err);
    });
}

function getAllUser(request) {
    var query = { role: 'user' };
    return User.count(query).then(function(count) {
        return User.find(query)
            .skip((request.pageIndex > 0) ? (request.pageIndex - 1) * request.pageSize : 0)
            .limit(request.pageSize)
            .exec().then(function(users) {
                for (var user in users) {
                    users[user].password = undefined;
                    users[user].salt = undefined;
                }
                var res = pagination.pagination(users, count, request.pageIndex, request.pageSize);
                return Promise.resolve(res);

            })
            .catch((err) => {
                return Promise.reject(err);
            });
    });
}

function getUserByEmail(email) {
    return User.findOne({ email: email })
        .then((user) => {
            if (!user) {
                return Promise.reject({
                    statusCode: 404,
                    message: failMessage.user.login.notFound
                });
            }
            return Promise.resolve({
                statusCode: 200,
                user: convertUserModelToUserResponse(user)
            });
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}

function deleteUser(id) {
    return User.remove({ _id: id })
        .then((user) => {
            return Promise.resolve({
                message: successMessage.user.remove
            });
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}