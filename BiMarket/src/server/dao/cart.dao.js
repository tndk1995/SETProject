/*jshint esversion: 6*/
var User = require('./../model/user.model');
var Product = require('./../model/product.model');
var Cart = require('./../model/cart.model');
var successMessage = require('../services/successMessage');
var failMessage = require('../services/failMessage');
var jwtDecode = require('jwt-decode');
var pagination = require('../services/pagination');

module.exports = {
    addCart: addCart,
    deleteCart: deleteCart,
    getCart: getCart
};
function getCart(request) {
    return Cart.count(request.query).then(function (count) {
        return Cart.find(request.query)
            .skip((request.pageIndex > 0) ? (request.pageIndex - 1) * request.pageSize : 0)
            .limit(request.pageSize)
            .exec().then(function (products) {
                var res = pagination.pagination(products, count, request.pageIndex, request.pageSize);
                return Promise.resolve(res);
            })
            .catch((err) => {
                return Promise.reject(err);
            });
    });
}
function addCart(request) {
    return Cart.find(request)
        .sort({ createDate: -1 })
        .limit(1)
        .then((cart) => {
            if (cart[0] && cart[0].status === 'available') {
                return Promise.reject({
                    statusCode: 500,
                    message: failMessage.cart.existed
                });
            }
            var newCart = new Cart({
                productId: request.productId,
                cardHolderId: request.cardHolderId,
                createDate: Date.now(),
                status: "available"
            });

            return newCart.save()
                .then((response) => {
                    return Promise.resolve({
                        message: successMessage.product.create,
                        product: response
                    });

                }).catch((err) => {
                    return Promise.reject({
                        statusCode: 500,
                        message: failMessage.product.systemErr
                    });
                });
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}

function deleteCart(request) {
    return Cart.find(request)
        .then((cart) => {
            if (!cart) {
                return Promise.reject({
                    statusCode: 404,
                    message: failMessage.cart.notFound
                });
            }
            cart.status = "unavailable";

            return cart.save()
                .then((res) => {
                    return Promise.resolve({
                        cart: res,
                        message: successMessage.product.update
                    });
                })
                .catch((err) => {
                    console.log(err);
                    return Promise.reject({
                        message: failMessage.cart.systemErr
                    });
                });
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}