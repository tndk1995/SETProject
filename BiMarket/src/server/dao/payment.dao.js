var Product = require('./../model/product.model');
var Payment = require('./../model/payment.model');
var successMessage = require('../services/successMessage');
var failMessage = require('../services/failMessage');
var config = require('../config/config');
var forge = require('node-forge');
var jwtDecode = require('jwt-decode');
var CryptoJS = require("crypto-js");
var requestService = require('request');
var jwt = require('./../utils/jwt.util');
var sendMail = require("./../utils/emailValidate");
var pagination = require('../services/pagination');

module.exports = {
    requestPayment: requestPayment,
    completePayment: completePayment,
    getAllReceipt: getAllReceipt
};

function getAllReceipt(request) {
    return Payment.count(request.query).then(function(count) {
        return Payment.find(request.query)
            .skip((request.pageIndex > 0) ? (request.pageIndex - 1) * request.pageSize : 0)
            .limit(request.pageSize)
            .exec().then(function(receipts) {
                var res = pagination.pagination(receipts, count, request.pageIndex, request.pageSize);
                return Promise.resolve(res);
            })
            .catch((err) => {
                return Promise.reject(err);
            });
    });
}

function requestPayment(request) {
    var newPayment = new Payment({
        typeOfPayment: request.typeOfPayment,
        dateOfRequest: request.dateOfRequest,
        cardHolderEmail: request.email,
        address: request.address,
        dateOfResponse: Date.now(),
        status: "request"
    });

    return newPayment.save()
        .then((response) => {
            var paymentToken = jwt.signTokenWithExpire({ paymentId: response._id }, 60 * 5);
            var payment = {
                _id: response._id,
                typeOfPayment: response.typeOfPayment,
                dateOfRequest: response.dateOfRequest,
                dateOfResponse: response.dateOfResponse,
                paymentToken: paymentToken,
                bankPubKeyToken: config.bankPubKeyToken.toString(),
                merchantPubKeyToken: config.merchantPubKeyToken.toString()
            }
            return Promise.resolve({
                payment: payment
            });

        }).catch((err) => {
            console.log(err);
            return Promise.reject({
                statusCode: 500,
                message: failMessage.payment.create
            });
        });
}

async function completePayment(request) {
    try {
        var OIMD = CryptoJS.SHA1(JSON.stringify(request.orderInformation)).toString();
        var POMD = CryptoJS.SHA1(OIMD + request.PIMD);

        var cardHolderPubKey = forge.pki.publicKeyFromPem(jwtDecode(request.cardHolderCert).publicKey);
        var md = forge.md.sha1.create();
        md.update(POMD);
        var resultComparePOMD = cardHolderPubKey.verify(md.digest().bytes(), request.DualSignature);
    } catch (err) {
        return Promise.reject({
            statusCode: 500,
            message: failMessage.payment.systemErr
        });
    }
    if (!resultComparePOMD) {
        return Promise.reject({
            statusCode: 500,
            message: failMessage.payment.dualSignature
        });
    } else {
        return Payment.findById(request.orderInformation.paymentId)
            .then((payment) => {
                if (!payment) {
                    return Promise.reject({
                        statusCode: 404,
                        message: failMessage.payment.notFound
                    });
                }

                var requestToBank = {
                    PIPacketCipher: request.PIPacketCipher,
                    DigitalEnvelope: request.DigitalEnvelope,
                    cardHolderCert: request.cardHolderCert,
                    merchantCert: config.merchantPubKeyToken,
                    // orderInformation: {
                    //     paymentId: request.orderInformation.paymentId,
                    //     totalCost: request.orderInformation.totalCost
                    // }
                };

                return new Promise(async(resolve, reject) => {
                    requestService.post({
                        url: 'https://localhost:8443/api/payment/request',
                        rejectUnauthorized: false,
                        form: requestToBank
                    }, async function(err, httpResponse, body) {
                        if (err) {
                            console.log(err);
                            return reject({
                                message: err
                            });
                        } else if (httpResponse.statusCode != 200) {
                            return reject(JSON.parse(body));
                        }

                        payment.products = await Promise.all(request.orderInformation.products.map((product) => {
                            return Product.findById(product._id).then((val) => {
                                return {
                                    productId: val._id,
                                    amount: product.amountToBuy,
                                    name: val.name,
                                    price: val.cost * product.amountToBuy,
                                }
                            });
                        }));
                        payment.totalCost = request.orderInformation.totalCost;

                        return payment.save()
                            .then((payment) => {
                                var orderInformation = {
                                    paymentId: request.orderInformation.paymentId,
                                    totalCost: request.orderInformation.totalCost
                                };
                                var requestPaymentToBank = {
                                    paymentToken: JSON.parse(body).message,
                                    encryptedOI: encryptOrderInfo(orderInformation),
                                    orderInformation: orderInformation
                                }
                                requestService.post({
                                    url: 'https://localhost:8443/api/payment',
                                    rejectUnauthorized: false,
                                    form: requestPaymentToBank
                                }, function(err2, httpResponse2, body2) {
                                    if (err2) {
                                        console.log(err2);
                                        return reject({
                                            message: err2
                                        });
                                    } else if (httpResponse2.statusCode != 200) {
                                        return reject(JSON.parse(body2));
                                    }
                                    var productUpdate = payment.products.map((product) => {
                                        return Product.update({
                                            "_id": product.productId
                                        }, {
                                            $inc: {
                                                "amount": -product.amount
                                            }
                                        });
                                    });
                                    payment.status = "completed";
                                    payment.save();
                                    return Promise.all(productUpdate).then((result) => {
                                        sendMail.mailPayment(payment.cardHolderEmail, payment);
                                        return resolve({
                                            message: successMessage.payment.complete
                                        })
                                    });
                                })

                            })
                            .catch((err) => {
                                console.log(err);
                                return reject({
                                    statusCode: 500,
                                    message: failMessage.payment.systemErr
                                })
                            })

                    })
                });
            })
            .catch((err) => {
                return Promise.reject(err);
            });
    }
}

function encryptOrderInfo(orderInfo) {
    var privateKey = forge.pki.privateKeyFromPem(config.merchantPrivateKey);
    var md = forge.md.sha1.create();
    md.update(orderInfo);
    var encrypted = privateKey.sign(md);
    return forge.util.encode64(encrypted);
}