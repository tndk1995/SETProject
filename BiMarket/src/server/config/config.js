var fs = require('fs');
var path = require('path');
var rootPath = __dirname;
var variables = JSON.parse(fs.readFileSync(path.join(rootPath, 'config.json'), 'utf8'));
variables.options = {
    key: fs.readFileSync(path.join(rootPath, 'server.key')),
    cert: fs.readFileSync(path.join(rootPath, 'server.crt'))
};
variables.certKey = fs.readFileSync(path.join(rootPath, 'mykey.pem'));
variables.pubKey = fs.readFileSync(path.join(rootPath, 'mykey.pub'));

variables.ca = fs.readFileSync(path.join(rootPath, 'server.csr'));
variables.merchantPrivateKey = fs.readFileSync(path.join(rootPath, 'MerchantPrivateKey.pem'));
variables.merchantPubKeyToken = fs.readFileSync(path.join(rootPath, 'merchantPubKeyToken.json'));
variables.bankPubKeyToken = fs.readFileSync(path.join(rootPath, 'bankPubKeyToken.json'));
module.exports = variables;