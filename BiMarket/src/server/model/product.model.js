var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    upload: {
        type: String
    },
    uploadType: {
        type: String
    },
    cost: {
        type: Number,
        required: true
    },
    amount: {
        type: Number
    },
    category: {
        type: String
    },
    createDate: {
        type: Date,
        default: Date.now
    },
    status: {
        type: String
    }
});

module.exports = mongoose.model('product', productSchema);