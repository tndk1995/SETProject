var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    merchantEmail: {
        type: String
    },
    salt: {
        type: String
    },
    username: {
        type: String
    },
    gender: {
        type: String
    },
    birthday: {
        type: Date
    },
    phone: {
        type: String
    },
    role: {
        type: String
    }
});

module.exports = mongoose.model('user', userSchema);;