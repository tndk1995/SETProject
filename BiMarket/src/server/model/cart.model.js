var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var cartSchema = new Schema({
    productId: {
        type: Schema.Types.ObjectId
    },
    cardHolderId: {
        type: Schema.Types.ObjectId
    },
    createDate: {
        type: Date
    },
    status: {
        type: String
    }
});

module.exports = mongoose.model('cart', cartSchema);