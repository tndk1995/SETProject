var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var paymentSchema = new Schema({
    typeOfPayment: {
        type: String
    },
    dateOfRequest: {
        type: Date
    },
    dateOfResponse: {
        type: Date
    },
    products: [{
        productId: { type: Schema.Types.ObjectId, ref: 'product' },
        amount: { type: Number },
        price: { type: Number },
        name: { type: String }
    }],
    cardHolderEmail: {
        type: String
    },
    address: {
        type: String
    },
    totalCost: {
        type: Number
    },
    status: {
        type: String
    }
});

module.exports = mongoose.model('payment', paymentSchema);