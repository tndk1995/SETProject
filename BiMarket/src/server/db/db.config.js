var config = require('./../config/config');
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://' + config.mongodb.host + '/' + config.mongodb.database);

// module.exports = mongoose.connection;