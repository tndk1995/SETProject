/*jshint esversion: 6*/
var router = require('express').Router();
var productDao = require('./../dao/product.dao');
var failMessage = require('./../services/failMessage');
var middlewareJwt = require('../middlewares/jwt-parser');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var fs = require('fs');
var path = require('path');

module.exports = function() {

    router.post('/get/:pageIndex', getProducts);
    router.get('/merchant/:pageIndex', middlewareJwt.parser('admin'), getProductsByMerchant);
    router.post('', middlewareJwt.parser('admin'), multipartMiddleware, createProduct);
    router.post('/update', middlewareJwt.parser('admin'), multipartMiddleware, updateProduct);
    router.post('/disable/:id', middlewareJwt.parser('admin'), disableProduct);


    function getProducts(req, res, next) {
        var request = {
            pageIndex: req.params.pageIndex,
            pageSize: 10,
            query: { status: 'available' }
        }
        if (req.body.category) {
            request.query.category = req.body.category;
        }
        productDao.getAllProduct(request)
            .then((response) => {
                res.status(200).send(response).end()
            }).catch((err) => {
                next(err);
            });
    }

    function disableProduct(req, res, next) {
        var request = {
            _id: req.params.id,
        }
        productDao.disableProduct(request)
            .then((response) => {
                res.status(200).send(response).end()
            }).catch((err) => {
                next(err);
            });
    }

    function getProductsByMerchant(req, res, next) {
        var request = {
            pageIndex: req.params.pageIndex,
            pageSize: 5,
            query: {}
        }
        productDao.getAllProduct(request)
            .then((response) => {
                res.status(200).send(response).end()
            }).catch((err) => {
                next(err);
            });
    }

    function createProduct(req, res, next) {
        var request = {
            name: req.body.name,
            cost: req.body.cost,
            amount: req.body.amount,
            category: req.body.category
        };
        var file;
        if (req.files) {
            file = req.files.file;
            request.uploadType = file.type.substring(6);
        };
        productDao.createProduct(request)
            .then((response) => {
                if (file) {
                    var fileId = response.product.id + '.' + response.product.uploadType;
                    var pathUpload = path.join(__dirname, '../upload/') + fileId;
                    fs.readFile(file.path, function(err, data) {
                        if (!err) {
                            fs.writeFile(pathUpload, data, function(err) {
                                if (!err) {
                                    res.status(200).send(response).end();
                                } else {
                                    next(err);
                                }
                            })
                        } else {
                            next(err);
                        }
                    });
                }

            })
            .catch((err) => {
                next(err);
            });


    }

    function updateProduct(req, res, next) {
        var request = {
            _id: req.body._id,
            name: req.body.name,
            cost: req.body.cost,
            amount: req.body.amount,
            category: req.body.category,
            decoded: req.decoded
        };
        var file;
        if (req.files.file) {
            file = req.files.file;
            request.uploadType = file.type.substring(6);
        };

        productDao.updateProduct(request)
            .then((response) => {
                if (file) {
                    var fileId = response.product._id + '.' + response.product.uploadType;
                    var pathUpload = path.join(__dirname, '../upload/') + fileId;
                    fs.readFile(file.path, function(err, data) {
                        if (!err) {
                            fs.writeFile(pathUpload, data, function(err) {
                                if (!err) {
                                    res.status(200).send(response).end();
                                } else {
                                    next(err);
                                }
                            })

                        } else {
                            next(err);
                        }
                    });
                } else {
                    res.status(200).send(response).end();
                }

            })
            .catch((err) => {
                next(err);
            });


    }

    return router;
};