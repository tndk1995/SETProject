/*jshint esversion: 6*/
var router = require('express').Router();
var userDao = require('./../dao/user.dao');
var middlewareJwt = require('../middlewares/jwt-parser');
var fs = require("fs");
var path = require('path');

module.exports = function() {
    router.get('/:pageIndex', middlewareJwt.parser('admin'), getAllUser);
    router.post('/create', middlewareJwt.parser('admin'), createUser);
    router.post('/setting', middlewareJwt.parser('admin'), settingServer);
    router.post('/change', middlewareJwt.parser('user', 'admin'), changePasswordUser);
    router.put('', middlewareJwt.parser('user', 'admin'), updateUser);
    router.get('', middlewareJwt.parser('user', 'admin'), getUser);
    router.delete('/:id', middlewareJwt.parser('admin'), deleteUser);

    function getUser(req, res, next) {
        userDao.getUserByEmail(req.decoded.email)
            .then((response) => {
                res.status(200).send(response).end()
            }).catch((err) => {
                next(err);
            });
    }

    function getAllUser(req, res, next) {
        var request = {
            pageIndex: req.params.pageIndex,
            pageSize: 5
        }
        userDao.getAllUser(request)
            .then((response) => {
                res.status(200).send(response).end()
            }).catch((err) => {
                console.log(err);
                next(err);
            });
    }

    function deleteUser(req, res, next) {
        userDao.deleteUser(req.params.id)
            .then((response) => {
                res.status(200).send(response).end()
            }).catch((err) => {
                next(err);
            });
    }

    function createUser(req, res, next) {
        var request = {
            email: req.body.email,
            password: req.body.password,
            username: req.body.username,
            birthday: req.body.birthday,
            gender: req.body.gender,
            position: req.body.position,
            address: req.body.address,
            phone: req.body.phone,
            roles: req.body.roles
        };

        if (request.email == "" || request.password == "" || request.username == "" || request.role == "") {
            res.status(403).send(failMessage.user.create.input).end();
        }
        userDao.createUser(request)
            .then((response) => {
                res.status(200).send(response).end()
            }).catch((err) => {
                next(err);
            });
    }

    function changePasswordUser(req, res, next) {
        var request = {
            passwordOld: req.body.passwordOld,
            passwordNew: req.body.passwordNew,
            token: req.body.token || req.query.token || req.headers['x-access-token']
        };

        if (request.passwordOld == "" || request.getPasswordNew == "") {
            console.log("403")
            return res.status(403).send(failMessage.user.changePassword.input).end()
        }
        userDao.changePasswordUser(request)
            .then((response) => {
                res.status(200).send(response).end()
            }).catch((err) => {
                next(err);
            })
    }

    function updateUser(req, res, next) {
        var request = {
            username: req.body.username,
            birthday: req.body.birthday,
            gender: req.body.gender,
            phone: req.body.phone,
            _id: req.body._id,
            decoded: req.decoded
        };
        if (req.body.oldPass || req.body.newPass) {
            request.oldPass = req.body.oldPass;
            request.newPass = req.body.newPass;
        }

        userDao.updateUser(request)
            .then((response) => {
                res.status(200).send(response).end();
            })
            .catch((err) => {
                next(err);
            });
    }

    async function settingServer(req, res, next) {
        var request = {
            email: req.body.email,
            _id: req.decoded._id
        };
        var pathServerConfig = path.join(__dirname, "../config/");
        var keyDone = await writeFile(pathServerConfig + "MerchantPrivateKey.pem", req.body.merchantPrivateKey);
        var merchantCertDone = await writeFile(pathServerConfig + "merchantPubKeyToken.json", req.body.merchantCert);
        var bankCertDone = await writeFile(pathServerConfig + "bankPubKeyToken.json", req.body.bankCert);
        if (!keyDone || !merchantCertDone || !bankCertDone) {
            next("err");
        }
        userDao.settingMerchant(request)
            .then((response) => {
                res.status(200).send(response).end();
            })
            .catch((err) => {
                next(err);
            });
    }
    return router;
};

function writeFile(fullpath, content) {
    return new Promise((resolve, reject) => {
        fs.writeFile(fullpath, content, (err) => {
            if (err) {
                console.log(err);
                return reject(false);
            }
            return resolve(true);
        })
    });
}





// function changePasswordUser(req, res, next) {
//     var request = {
//         passwordOld: req.body.passwordOld,
//         passwordNew: req.body.passwordNew,
//         token: req.body.token || req.query.token || req.headers['x-access-token']
//     };
//     userDao.changePasswordUser(request, function(err, response) {
//         if (err) {
//             next(err);
//         } else {
//             res.status(200).send(response).end();
//         }
//     });
// }