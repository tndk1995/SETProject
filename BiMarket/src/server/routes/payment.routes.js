/*jshint esversion: 6*/
var router = require('express').Router();
var paymentDao = require('./../dao/payment.dao');
var failMessage = require('./../services/failMessage');
var middlewareJwt = require('../middlewares/jwt-parser');
var paymentParser = require('../middlewares/payment-jwt-parser');
var forge = require('node-forge');

module.exports = function() {

    router.get('/:pageIndex', middlewareJwt.parser('user', 'admin'), getAllReceipt);
    router.post('', middlewareJwt.parser('user'), requestPayment);
    router.post('/complete', middlewareJwt.parser('user'), paymentParser.parser(), completePayment);
    // router.delete('', middlewareJwt.parser('cardHolder'), deleteCart);


    function requestPayment(req, res, next) {
        var request = {
            typeOfPayment: req.body.typeOfPayment,
            dateOfRequest: req.body.dateOfRequest,
            address: req.body.address,
            email: req.decoded.email
        }
        paymentDao.requestPayment(request)
            .then((response) => {
                res.status(200).send(response).end()
            }).catch((err) => {
                next(err);
            });
    }

    function completePayment(req, res, next) {
        var request = {
            PIPacketCipher: req.body.PIPacketCipher,
            DigitalEnvelope: forge.util.decode64(req.body.DigitalEnvelope),
            PIMD: req.body.PIMD,
            orderInformation: JSON.parse(req.body.orderInformation),
            DualSignature: forge.util.decode64(req.body.DualSignature),
            cardHolderCert: req.body.cardHolderCert
        }
        paymentDao.completePayment(request)
            .then((response) => {
                res.status(200).send(response).end()
            }).catch((err) => {
                next(err);
            });
    }

    function getAllReceipt(req, res, next) {
        var request = {
            pageIndex: req.params.pageIndex,
            pageSize: 10,
            query: { status: "completed" }
        }
        if (req.decoded.role === "user") {
            request.query.cardHolderEmail = req.decoded.email;
        }
        paymentDao.getAllReceipt(request)
            .then((response) => {
                res.status(200).send(response).end()
            }).catch((err) => {
                next(err);
            });
    }
    return router;
};