/*jshint esversion: 6*/
var router = require('express').Router();
var cartDao = require('./../dao/cart.dao');
var failMessage = require('./../services/failMessage');
var middlewareJwt = require('../middlewares/jwt-parser');
var jwtDecode = require('jwt-decode');

module.exports = function () {

    router.get('/:pageIndex', middlewareJwt.parser('cardHolder'), getCart);
    router.post('', middlewareJwt.parser('cardHolder'), addCart);
    router.delete('', middlewareJwt.parser('cardHolder'), deleteCart);


    function getCart(req, res, next) {
        var token = jwtDecode(req.headers['x-access-token']);
        var request = {
            pageIndex: req.params.pageIndex,
            pageSize: 5,
            query: { cardHolderId: token._id, status: 'available' }
        }
        cartDao.getCart(request)
            .then((response) => {
                res.status(200).send(response).end()
            }).catch((err) => {
                next(err);
            });
    }

    function addCart(req, res, next) {
        var token = jwtDecode(req.headers['x-access-token']);
        var request = {
            productId: req.body.productId,
            cardHolderId: token._id
        };

        cartDao.addCart(request)
            .then((response) => {
                res.status(200).send(response).end();
            })
            .catch((err) => {
                next(err);
            });
    }
    function deleteCart(req, res, next) {
        var token = jwtDecode(req.headers['x-access-token']);
        var request = {
            productId: req.body.productId,
            cardHolderId: token._id
        };

        cartDao.addCart(request)
            .then((response) => {
                res.status(200).send(response).end();
            })
            .catch((err) => {
                next(err);
            });
    }

    return router;
};