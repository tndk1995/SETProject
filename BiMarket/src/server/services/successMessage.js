var user = {
    login: 'LOGIN_SUCESS',
    register: 'REGISTER_SUCCESS',
    signup: 'CREATE_SUCCESS',
    changePassword: 'CHANGE_SUCCESS',
    sendEmail: 'SEND_SUCCESS',
    captcha: 'CAPTCHA_SUCCESS',
    update: 'UPDATE_SUCCESS',
    remove: 'REMOVE_SUCCESS'
};
var product = {
    create: 'Create successfully',
    update: 'Update sucessfully',
    disable: 'Disabled sucessfully'
}
var payment = {
    complete: "Your payment is completed"
}
module.exports = {
    user: user,
    product: product,
    payment: payment
};