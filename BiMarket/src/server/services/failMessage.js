var user = {
    login: {
        input: 'ERROR_INPUT',
        systemErr: 'SYSTEM_ERROR',
        notFound: 'USER_NOT_FOUND',
        inCorrect: 'PASSWORD_INCORRECT',
        over5Time: 'OVER_TIME',
        inProgress: 'IN_PROGRESS'
    },
    signup: {
        input: 'ERROR_INPUT',
        systemErr: 'SYSTEM_ERROR',
        duplicateUser: 'DUPLICATE_USER'
    },
    register: {
        input: 'ERROR_INPUT'
    },
    changePassword: {
        systemErr: 'SYSTEM_ERROR',
        passwordOldNotCorrect: 'PASSWORD_OLD_NOT_CORRECT',
        input: 'ERROR_INPUT'
    },
    captcha: {
        noCaptcha: "NO_CAPTCHA",
        failCaptcha: "FAIL_CAPTCHA"
    },
    jwt: {
        unauthorized: "UN_AUTHORIZED",
        noToken: "NO_TOKEN",
        expired: "TOKEN_EXPIRED"
    }
};
var product = {
    create: "Fail to create product",
    notFound: "Product is not found",
    systemErr: 'The system is error'
}
var cart = {
    create: "Fail to create cart",
    notFound: "Cart is not found",
    existed: "This product already in your cart",
    systemErr: 'The system is error'
}
var payment = {
    create: "Fail to create payment",
    notFound: "Payment is not found",
    inProgress: "Payment is progressing",
    noToken: "Payment has no token",
    dualSignature: "Fail to verify Dual Signature ",
    systemErr: 'The system is error'
}
module.exports = {
    user: user,
    product: product,
    cart: cart,
    payment: payment
};