'use strict';
var jwt = require('jsonwebtoken');
var config = require('../config/config');
module.exports = {
    signToken: signToken,
    signTokenWithExpire: signTokenWithExpire
};

function signToken(information, state) {
    if (state === "0") {
        var token = jwt.sign(information, config.certKey, { algorithm: 'RS256', expiresIn: '1d' });
    } else {
        var token = jwt.sign(information, config.certKey, { algorithm: 'RS256', expiresIn: '7d' });
    }
    return token;
}

function signTokenWithExpire(information, expireTime) {
    return jwt.sign(information, config.certKey, { algorithm: 'RS256', expiresIn: expireTime });
}