/*jshint esversion: 6*/
var nodemailer = require('nodemailer');
var transport = require('nodemailer-smtp-transport');
var fs = require("fs");
var path = require("path");
var transporter = nodemailer.createTransport(transport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
        user: 'biaiteam12@gmail.com',
        pass: 'Biaiteam12'
    }
}));
var rootPath = path.join(__dirname, "emailTemplate");
module.exports = {
    sendMail: sendMail,
    mailPayment: mailPayment
};

function sendMail(email, token) {
    var mailOptions = {
        from: '"BiAi" <biaiteam12@gmail.com>',
        to: email,
        subject: 'Reset Password Vertification',
        html: 'http://localhost:4000/resetPassword/' + token
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });
}

function mailPayment(email, paymentInfo) {
    var htmlTemplate = fs.readFileSync(path.join(rootPath, "receipt.html"), "utf8");
    var dataContent = fs.readFileSync(path.join(rootPath, "dataContent.html"), "utf8");
    var dataContents = "";
    paymentInfo.products.forEach((product) => {
        dataContents += dataContent.replace("$productCost", product.price)
            .replace("$productName", product.name)
            .replace("$productAmount", product.amount);
    });
    htmlTemplate = htmlTemplate.replace("$address", paymentInfo.address)
        .replace("$dataContent", dataContents)
        .replace("$totalCost", paymentInfo.totalCost)
        .replace("$dateOfRequest", paymentInfo.dateOfRequest);

    var mailOptions = {
        from: '"Camera Store" <biaiteam12@gmail.com>',
        to: email,
        subject: 'Online receipt',
        html: htmlTemplate
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });
}