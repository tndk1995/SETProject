var jwt = require('jsonwebtoken');
var config = require('../config/config');
var Payment = require('./../model/payment.model');
var failMessage = require('./../services/failMessage');
module.exports = {
    parser: parser
};

function parser() {
    var role = Array.prototype.slice.call(arguments);
    return function(req, res, next) {
        var paymentToken = req.body.paymentToken;
        // decode token
        if (paymentToken) {
            // verifies secret and checks exp
            return new Promise(function(resolve, reject) {
                jwt.verify(paymentToken, config.pubKey, { algorithms: ['RS256'] }, function(err, decoded) {
                    if (err) {
                        console.log(err);
                        if (err.name == 'TokenExpiredError') {
                            return reject(res.status(500).send({ message: failMessage.user.jwt.expired }).end());
                        }
                        return reject(res.status(500).send({ message: failMessage.user.jwt.unauthorized }).end());
                    } else {
                        var paymentId = decoded.paymentId;
                        Payment.findById(paymentId).then((payment) => {
                            if (!payment) {
                                return reject(res.status(500).send({ message: failMessage.payment.notFound }).end());
                            } else if (payment.status == "processing") {
                                return reject(res.status(500).send({ message: failMessage.payment.inProgress }).end());
                            }
                            payment.status = "processing";
                            payment.save()
                                .then((res) => {
                                    next()
                                })
                                .catch(err => {
                                    return reject(err);
                                })

                        })
                    }
                });
            });
        } else {
            // if there is no token
            return res.status(403).send({
                message: failMessage.payment.noToken
            });
        }
    }
}